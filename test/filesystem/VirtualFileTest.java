package filesystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exceptions.FileInvalidLineException;

public class VirtualFileTest {
    private VirtualFile file;

    @Before
    public void setData() {
        file = new VirtualFile("Test");
    }

    public String changeLineEncoding(String text, Charset oldCharset, Charset newCharset) {
        byte bytes[] = text.getBytes(oldCharset);
        return new String(bytes, newCharset);
    }

    @Test
    public void When_WriteToFileOnLineInBounds_Expect_LineToContainCharacters() throws FileInvalidLineException {

        file.write(1, "abcd");
        assertFalse(file.getLine(1).isEmpty());
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteToFileOutOfLowerBound_Expect_ToReturnFalse() throws FileInvalidLineException {
        assertFalse(file.write(-5, "abc"));
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteToFileOutOfUpperBound_Expect_ToReturnFalse() throws FileInvalidLineException {
        assertFalse(file.write(20, "abc"));
    }

    @Test
    public void When_WriteToFileInBounds_Expect_CharactersLengthToBeAddedToFileSize() throws FileInvalidLineException {

        file.write(1, "abcd");
        assertEquals(4, file.getLine(1).length());
    }

    @Test
    public void When_WriteToFileInBoundsWhichAlreadyContainsText_Expect_NewLengthToBeAddedToFileSizeAndOldLengthToBeSubstracted()
            throws FileInvalidLineException {

        file.write(1, "abcd");
        assertEquals(4, file.getLine(1).length());
        file.write(1, "hi");
        assertEquals(2, file.getLine(1).length());
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_GetALineOutOfLowerBound_Expect_AnExceptionToBeThrown() throws FileInvalidLineException {
        file.getLine(-5);

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_GetALineOutOfUpperBound_Expect_AnExceptionToBeThrown() throws FileInvalidLineException {
        file.getLine(20);

    }

    @Test
    public void When_GetALineInBoundsAndStringIsEmpty_Expect_ToReturnEmptyString() throws FileInvalidLineException {
        file.write(1, "abcd");
        assertEquals("abcd", file.getLine(1));

    }

    @Test
    public void When_FileToWhichCompareHasTheSameName_Expect_ToReturnZero() {
        VirtualFile file2 = new VirtualFile("Test");
        assertEquals(0, file.compareTo(file2));
    }

    @Test
    public void When_FileToWhichCompareHasNameLessAccordingToEnglishAlphabet_Expect_ToReturnOne() {
        VirtualFile file2 = new VirtualFile("Abc");
        assertEquals(1, file.compareTo(file2));
    }

    @Test
    public void When_FileToWhichCompareHasNameGreaterAccordingToEnglishAlphabet_Expect_ToReturnMinusOne() {
        VirtualFile file2 = new VirtualFile("Zbc");
        assertEquals(-1, file.compareTo(file2));
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_InputIndexIsAboveUpperBound_Expect_AnExceptionToBeThrown() throws FileInvalidLineException {
        file.getLine(20);
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_InputIndexIsUnderLowerBound_Expect_AnExceptionToBeThrown() throws FileInvalidLineException {
        file.getLine(-5);
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_InputIndexIsZero_Expect_AnExceptionToBeThrown() throws FileInvalidLineException {

        file.getLine(0);
    }

    @Test
    public void When_InputIndexIsInBounds_Expect_ToReturnLineContent() throws FileInvalidLineException {
        file.write(1, "abc");
        assertEquals("abc", file.getLine(1));
    }

    @Test
    public void When_WriteToFileInEncodingDifferentThanCurrent_Expect_ToReturnContentToBeEncodedUsingInput()
            throws FileInvalidLineException {
        file.write(1, "abc");
        file.write(2, "абв");
        file.write(3, "АБВ", StandardCharsets.US_ASCII);
        List<String> content = file.getContent();
        List<String> contentExpected = Arrays.asList(
                changeLineEncoding("abc", StandardCharsets.UTF_8, StandardCharsets.US_ASCII),
                changeLineEncoding("абв", StandardCharsets.UTF_8, StandardCharsets.US_ASCII),
                changeLineEncoding("АБВ", StandardCharsets.UTF_8, StandardCharsets.US_ASCII));

        assertEquals(contentExpected, content);

    }

    @Test
    public void When_WriteToFileInEncodingSameAsCurrent_Expect_ToReturnContentToBeEncodedUsingInput()
            throws FileInvalidLineException {
        file.write(1, "abc");
        file.write(2, "абв");
        file.write(3, "АБВ", StandardCharsets.UTF_8);
        List<String> content = file.getContent();
        List<String> contentExpected = Arrays.asList("abc", "абв", "АБВ");

        assertEquals(contentExpected, content);

    }

    @Test
    public void When_SetFileEncodingSameAsCurrent_Expect_ToReturnContentToBeEncodedUsingInput()
            throws FileInvalidLineException {
        file.write(1, "abc");
        file.write(2, "абв");
        file.setEncoding(StandardCharsets.UTF_8);
        List<String> content = file.getContent();
        List<String> contentExpected = Arrays.asList("abc", "абв");

        assertEquals(contentExpected, content);
    }

    @Test
    public void When_SetFileEncodingToDifferent_Expect_ToReturnContentToBeEncodedUsingInput()
            throws FileInvalidLineException {
        file.write(1, "abc");
        file.write(2, "абв");
        file.setEncoding(StandardCharsets.US_ASCII);
        List<String> content = file.getContent();
        List<String> contentExpected = Arrays.asList(
                changeLineEncoding("abc", StandardCharsets.UTF_8, StandardCharsets.US_ASCII),
                changeLineEncoding("абв", StandardCharsets.UTF_8, StandardCharsets.US_ASCII));

        assertEquals(contentExpected, content);

    }

}
