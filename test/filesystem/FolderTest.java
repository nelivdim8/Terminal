package filesystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exceptions.FileInvalidLineException;

public class FolderTest {
    private Folder folder;

    @Before
    public void setData() {
        folder = new Folder("Test");
    }

    @Test
    public void When_AddSubFolder_Expect_FolderToContainCurrentSubfolder() {
        folder.addSubfolder(new Folder("Subfolder"));
        assertTrue(folder.getSubfolders().containsKey("Subfolder"));
    }

    @Test
    public void When_AddSubFolder_Expect_SubfolderParentToBeCurrentFolder() {

        folder.addSubfolder(new Folder("Subfolder"));
        assertEquals(folder, folder.getSubFolder("Subfolder").getParent());

    }

    @Test
    public void When_AddFilesNamesByDescendingOrder_Expect_ToBePresentedInListAndSortedAscenging() {
        VirtualFile file1 = new VirtualFile("File1");
        VirtualFile file2 = new VirtualFile("File2");
        VirtualFile file3 = new VirtualFile("File3");
        folder.addFile(file3);
        folder.addFile(file2);
        folder.addFile(file1);
        List<VirtualFile> files = Arrays.asList(new VirtualFile[] { file1, file2, file3 });
        assertEquals(files, folder.listFiles());

    }

    @Test
    public void When_AddFilesNamesByAcendingOrder_Expect_ToBePresentedInListAndSortedAscenging() {
        VirtualFile file1 = new VirtualFile("File1");
        VirtualFile file2 = new VirtualFile("File2");
        VirtualFile file3 = new VirtualFile("File3");
        folder.addFile(file1);
        folder.addFile(file2);
        folder.addFile(file3);
        List<VirtualFile> files = Arrays.asList(new VirtualFile[] { file1, file2, file3 });
        assertEquals(files, folder.listFiles());

    }

    @Test
    public void When_FolderDoesNotContainAnyFiles_Expect_ListFilesToReturnEmptyList() {

        assertEquals(new ArrayList<>(), folder.listFiles());

    }

    @Test
    public void When_FolderDoesNotContainAnyFiles_Expect_ListSortedFilesToReturnEmptyList() {

        assertEquals(new ArrayList<>(), folder.listSortedFiles());

    }

    @Test
    public void When_WriteToFiles_Expect_ToReturnThemSortedBySizeInDescensingOrder() throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("File1");
        VirtualFile file2 = new VirtualFile("File2");
        VirtualFile file3 = new VirtualFile("File3");
        file3.write(1, "a");
        file2.write(1, "aaaaaaaaaaaaaaa");
        file1.write(1, "aaaaaa");
        folder.addFile(file1);
        folder.addFile(file2);
        folder.addFile(file3);
        List<VirtualFile> files = Arrays.asList(new VirtualFile[] { file2, file1, file3 });
        assertEquals(files, folder.listSortedFiles());

    }

    @Test
    public void When_WriteEqualNumberOfCharactersInSomeOfFiles_Expect_ToReturnThemSortedBySizeInDescensingOrder()
            throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("File1");
        VirtualFile file2 = new VirtualFile("File2");
        VirtualFile file3 = new VirtualFile("File3");
        file3.write(1, "a");
        file2.write(1, "aaaaaaaaaaaaaaa");
        file1.write(1, "a");
        folder.addFile(file1);
        folder.addFile(file2);
        folder.addFile(file3);
        List<VirtualFile> files = Arrays.asList(new VirtualFile[] { file2, file1, file3 });
        assertEquals(files, folder.listSortedFiles());

    }

    @Test
    public void When_FolderContainsCurrentSubfolder_Expect_ToReturnTrue() {

        folder.addSubfolder("Sub");
        assertTrue(folder.containsSubFolder("Sub"));

    }

    @Test
    public void When_FolderDoesNotContainCurrentSubfolder_Expect_ToReturnFalse() {

        folder.addSubfolder("Sub1");
        assertFalse(folder.containsSubFolder("Sub"));

    }

    @Test
    public void When_FolderContainsCurrentFile_Expect_ToReturnTrue() {

        folder.addFile("File");
        assertTrue(folder.containsFile("File"));

    }

    @Test
    public void When_FolderDoesNotContainCurrentFile_Expect_ToReturnFalse() {

        folder.addFile("File1");
        assertFalse(folder.containsFile("File"));

    }

    @Test
    public void When_FolderDoesNotContainAnyFiles_Expect_ToReturnFalse() {

        assertFalse(folder.containsFiles());

    }

    @Test
    public void When_FolderContainsFiles_Expect_ToReturnTrue() {

        folder.addFile("File1");
        folder.addFile("File2");
        folder.addFile("File3");
        assertTrue(folder.containsFiles());

    }

    @Test
    public void When_FolderDoesNotContainAnySubfolders_Expect_ToReturnFalse() {

        assertFalse(folder.containsSubFolders());

    }

    @Test
    public void When_FolderContainsSubfolders_Expect_ToReturnTrue() {

        folder.addSubfolder("Folder1");
        folder.addSubfolder("Folder2");
        folder.addSubfolder("Folder3");
        assertTrue(folder.containsSubFolders());

    }

    @Test
    public void When_TryToGetASubfolderWhichFolderDoesNotContain_Expect_ToReturnNull() {

        assertNull(folder.getSubFolder("Folder"));

    }

    @Test
    public void When_TryToGetASubfolderWhichFolderContains_Expect_NotToReturnNull() {
        folder.addSubfolder("Folder");
        assertNotNull(folder.getSubFolder("Folder"));

    }

    @Test
    public void When_TryToGetAFileWhichFolderContains_Expect_NotToReturnNull() {

        folder.addFile("File");
        assertNotNull(folder.getFile("File"));

    }

    @Test
    public void When_TryToGetAFileWhichFolderDoesNotContain_Expect_ToReturnNull() {

        assertNull(folder.getFile("File"));

    }

    @Test
    public void When_AddFileToFolderUsingNameArgument_Expect_FolderToContainTheFile() {
        folder.addFile("File");
        assertTrue(folder.containsFile("File"));

    }

    @Test
    public void When_AddFileUsingFileArgumentToFolder_Expect_FolderToContainTheFile() {
        folder.addFile(new VirtualFile("Abc"));
        assertTrue(folder.containsFile("Abc"));

    }

    @Test
    public void When_AddSubfolderUsingFolderArgumentToFolder_Expect_FolderToContainTheSubfolder() {
        folder.addSubfolder(new Folder("Subfolder"));
        assertTrue(folder.containsSubFolder("Subfolder"));

    }

    @Test
    public void When_AddSubfolderUsingNameArgumentToFolder_Expect_FolderToContainTheSubfolder() {
        folder.addSubfolder("Subfolder");
        assertTrue(folder.containsSubFolder("Subfolder"));

    }
}
