package filesystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import exceptions.FileInvalidLineException;

public class SizeComparatorTest {
    private SizeComparator comparator;

    @Before
    public void setData() {
        comparator = new SizeComparator();

    }

    @Test
    public void When_FirstFileSizeIsGreater_Expect_ToReturnNegativeNumber() throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("Test1");
        VirtualFile file2 = new VirtualFile("Test2");
        file1.write(1, "abcabcabc");
        file2.write(1, "abc");
        assertTrue(comparator.compare(file1, file2) < 0);

    }

    @Test
    public void When_FirstFileSizeIsLess_Expect_ToReturnPositiveNumber() throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("Test1");
        VirtualFile file2 = new VirtualFile("Test2");
        file2.write(1, "abcabcabc");
        file1.write(1, "abc");
        assertTrue(comparator.compare(file1, file2) > 0);

    }

    @Test
    public void When_FirstFileSizeIsEqualToSecondAndNamesAreEqual_Expect_ToReturnZero()
            throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("Test1");
        VirtualFile file2 = new VirtualFile("Test1");
        file2.write(1, "abc");
        file1.write(1, "abc");
        assertEquals(0, comparator.compare(file1, file2));

    }

    @Test
    public void When_FirstFileSizeIsEqualToSecondFirstNameIsGreater_Expect_ToReturnNegativeNumber()
            throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("Abb");
        VirtualFile file2 = new VirtualFile("Baa");
        file1.write(1, "abc");
        file2.write(1, "abc");
        assertTrue(comparator.compare(file1, file2) < 0);

    }

    @Test
    public void When_FirstFileSizeIsEqualToSecondFirstNameIsLess_Expect_ToReturnPositiveNumber()
            throws FileInvalidLineException {
        VirtualFile file1 = new VirtualFile("Baa");
        VirtualFile file2 = new VirtualFile("Abb");
        file1.write(1, "abc");
        file2.write(1, "abc");
        assertTrue(comparator.compare(file1, file2) > 0);

    }

}
