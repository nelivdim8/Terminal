package filesystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import exceptions.FileInvalidLineException;
import exceptions.HardDriveOutOfStorageException;
import exceptions.UnexistingFileException;

public class VirtualFileSystemTest {
    private VirtualFileSystem fileSystem;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
    }

    @Test
    public void When_CurrentFolderIsRoot_Expect_GetCurrentFolderNameToReturnCorrectPath() {
        String path = "/home/";
        assertEquals(path, fileSystem.getCurrentFolderName());
    }

    @Test
    public void When_TryToMakeDirectoryWithDuplicatedName_Expect_mkDirToReturnFalse() {

        fileSystem.mkDir("NewFolder");
        assertFalse(fileSystem.mkDir("NewFolder"));
    }

    @Test
    public void When_TryToMakeDirectoryWithSameNameAsParent_Expect_mkDirToReturnFalse() {

        assertFalse(fileSystem.mkDir("home"));
    }

    @Test
    public void When_TryToMakeDirectoryWithNoDuplicatedName_Expect_mkDirToReturnTrue() {

        assertTrue(fileSystem.mkDir("Unique"));
    }

    @Test
    public void When_MakeDirectoryInRootDrectory_Expect_ParentOfSubdirectoryToBeRootAndRootToContainsNewDirectoryIntsList() {
        fileSystem.mkDir("Unique");
        assertTrue(fileSystem.getCurrentFolder().getSubfolders().containsKey("Unique"));
        assertEquals(fileSystem.getCurrentFolder(),
                fileSystem.getCurrentFolder().getSubfolders().get("Unique").getParent());
    }

    @Test
    public void When_MakeDirectoryInSubdirectoryOfRoot_Expect_CorrectHierarchyOfFolder() {
        fileSystem.mkDir("Unique");
        fileSystem.changeDir("Unique");
        fileSystem.mkDir("Subdirectory");
        System.out.println(fileSystem.getCurrentFolder().getName());
        assertTrue(fileSystem.getCurrentFolder().getName().equals("Unique"));
        assertTrue(fileSystem.getCurrentFolder().getParent().getName().equals("home"));
        assertNotNull(fileSystem.getCurrentFolder().getSubfolders().get("Subdirectory"));

    }

    @Test
    public void When_ChangeDirectoryToExistingSubdirectory_Expect_chandeDirToReturnTrue() {
        fileSystem.mkDir("Unique");
        assertTrue(fileSystem.changeDir("Unique"));

    }

    @Test
    public void When_CurrentDirectoryIsRootAndTryToChangeItToParnt_Expect_ReturnFalse() {
        fileSystem.changeDir("..");
        assertFalse(fileSystem.changeDir(".."));

    }

    @Test
    public void When_ChangeDirectoryToNonExistingSubdirectory_Expect_chandeDirToReturnFalse() {

        assertFalse(fileSystem.changeDir("Unique"));

    }

    @Test
    public void When_ChangeRootDirectoryToParent_Expect_chandeDirToReturnFalse() {

        assertTrue(fileSystem.changeDir(".."));

    }

    @Test
    public void When_ChangeRootDirectoryToCurrent_Expect_chandeDirToReturnTrue() {

        assertTrue(fileSystem.changeDir("."));

    }

    @Test
    public void When_ChangeSubdirectoryOfRootDirectoryToCurrent_Expect_chandeDirToReturnTrue() {
        fileSystem.mkDir("Unique");
        assertTrue(fileSystem.changeDir("."));

    }

    @Test
    public void When_ChangeSubdirectoryOfRootDirectoryToParent_Expect_chandeDirToReturnTrue() {
        fileSystem.mkDir("Unique");
        assertTrue(fileSystem.changeDir(".."));

    }

    @Test
    public void When_CreateFileInRootDirectory_Expect_createFileToReturnTrue() throws HardDriveOutOfStorageException {
        assertTrue(fileSystem.createFile("New File.txt"));

    }

    @Test
    public void When_CreateFileWithAlreadyExistingNameInRootDirectory_Expect_createFileToReturnFalse()
            throws HardDriveOutOfStorageException {
        fileSystem.createFile("Unique");
        assertFalse(fileSystem.createFile("Unique"));

    }

    @Test
    public void When_CreateFileInRootDirectory_Expect_correctHierarchy() throws HardDriveOutOfStorageException {
        fileSystem.createFile("New File.txt");
        assertTrue(fileSystem.getCurrentFolder().getFiles().containsKey("New File.txt"));

    }

    @Test
    public void When_CreateFileInSubdirectoryOfRootDirectory_Expect_correctHierarchy()
            throws HardDriveOutOfStorageException {
        fileSystem.mkDir("Subfolder");
        fileSystem.changeDir("Subfolder");
        fileSystem.createFile("New File.txt");
        assertTrue(fileSystem.getCurrentFolder().getFiles().containsKey("New File.txt"));
    }

    @Test
    public void When_CreateFileInSubdirectoryOfRootDirectory_Expect_FileSizeToBeZero()
            throws HardDriveOutOfStorageException {
        fileSystem.mkDir("Subfolder");
        fileSystem.changeDir("Subfolder");
        fileSystem.createFile("New File.txt");
        assertEquals(0, fileSystem.getCurrentFolder().getFiles().get("New File.txt").getSize());

    }

    @Test
    public void When_WriteInFile_Expect_FileSizeToBeLinesNumberPlusCharactersNumberOnEachLine() throws IOException {

        fileSystem.createFile("New File.txt");
        fileSystem.writeToFile("New File.txt", 1, "Hello World lq");
        assertEquals(15, fileSystem.getCurrentFolder().getFile("New File.txt").getSize());

    }

    @Test
    public void When_WriteInFileOnLineWhichContainCharacters_Expect_FileSizeToBecameNewTextLengthPlusLinesNumber()
            throws IOException {

        fileSystem.createFile("New File.txt");
        fileSystem.writeToFile("New File.txt", 1, "Hello World lq");
        assertEquals(15, fileSystem.getCurrentFolder().getFiles().get("New File.txt").getSize());
        fileSystem.writeToFile("New File.txt", 1, "Hello");
        assertEquals(6, fileSystem.getCurrentFolder().getFiles().get("New File.txt").getSize());

    }

    @Test(expected = HardDriveOutOfStorageException.class)
    public void When_TryToWriteInFileButDiskSpaceIsFull_Expect_AHardDriveOutOfStorageExceptionToBeThrown()
            throws IOException {

        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "Hello World");

        fileSystem.writeToFile("File.txt", 2, "Hello");
        fileSystem.writeToFile("File.txt", 3, "Hello");

    }

    @Test
    public void When_TryToWriteInFileButDiskSpaceIsFullBinIsNotEmptyAndItsMemoryIsGreaterThanNewText_Expect_BinToBeClearedAndItsMemoryFreed()
            throws IOException {
        fileSystem.createFile("File1.txt");
        fileSystem.createFile("File2.txt");
        fileSystem.writeToFile("File1.txt", 1, "Hello World");
        fileSystem.writeToFile("File2.txt", 1, "Hello");
        fileSystem.createFile("File3.txt");
        fileSystem.writeToFile("File3.txt", 1, "H");
        fileSystem.remove("File1.txt");
        assertTrue(fileSystem.writeToFile("File3.txt", 2, "1234567"));

    }

    @Test
    public void When_TryToWriteInFileButDiskSpaceIsFullBinIsNotEmptyAndItsMemoryIsEqualToNewText_Expect_BinToBeClearedAndItsMemoryFreed()
            throws IOException {
        fileSystem.createFile("File1.txt");
        fileSystem.createFile("File2.txt");
        fileSystem.writeToFile("File1.txt", 1, "1234567");
        fileSystem.writeToFile("File2.txt", 1, "Hello");
        fileSystem.createFile("File3.txt");
        fileSystem.writeToFile("File3.txt", 1, "Hello");
        fileSystem.remove("File1.txt");
        assertTrue(fileSystem.writeToFile("File3.txt", 2, "1234567"));

    }

    @Test
    public void When_WriteInFileAndDiskSpaceIsFullButNewContentToBeOverwrittenisLessthanDiskSize_Expect_BinToBeClearedAndItsMemoryFreed()
            throws IOException {
        fileSystem.createFile("File1.txt");
        fileSystem.writeToFile("File1.txt", 1, "123456789123456789");
        assertTrue(fileSystem.writeToFile("File1.txt", 1, "1234567"));

    }

    @Test(expected = HardDriveOutOfStorageException.class)
    public void When_TryToWriteContentWhichMemoryExceedsHardDiskCapacity_Expect_AHardDriveOutOfStorageException()
            throws IOException {
        fileSystem.createFile("File1.txt");
        assertTrue(fileSystem.writeToFile("File1.txt", 1, "12345678912345678910"));

    }

    @Test(expected = HardDriveOutOfStorageException.class)
    public void When_TryToWriteInFileButDiskSpaceIsFullBinIsNotEmptyAndItsMemoryIsLessThanNewText_Expect_BinToBeClearedAndItsMemoryFreed()
            throws IOException {
        fileSystem.createFile("File1.txt");
        fileSystem.createFile("File2.txt");
        fileSystem.writeToFile("File1.txt", 1, "1234567");
        fileSystem.writeToFile("File2.txt", 1, "Hello");
        fileSystem.createFile("File3.txt");
        fileSystem.writeToFile("File3.txt", 1, "Hello");
        fileSystem.remove("File1.txt");
        assertTrue(fileSystem.writeToFile("File3.txt", 2, "12345678"));

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteInFileDirectoryGreaterThanBounds_Expect_FileInvalidLineExceptionToBeThrown()
            throws IOException {

        fileSystem.createFile("New File.txt");
        assertFalse(fileSystem.writeToFile("New File.txt", 20, "Hello World lq"));

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteInFileDirectoryLessThanBounds_Expect_FileInvalidLineExceptionToBeThrown() throws IOException {

        fileSystem.createFile("New File.txt");
        assertFalse(fileSystem.writeToFile("New File.txt", -5, "Hello World lq"));

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteInFile_Expect_FileInvalidLineExceptionToBeThrown() throws IOException {
        String content = "Hello World lq";
        fileSystem.createFile("New File.txt");
        fileSystem.writeToFile("New File.txt", 2, "Hello World lq");
        assertEquals(content, fileSystem.getCurrentFolder().getFiles().get("New File.txt").getContent().get(1));

    }

    @Test
    public void When_WriteInFileOnLineWhichContainCharacters_Expect_ContentToBeChangedWithNewInput()
            throws IOException {
        String newContent = "Java";
        fileSystem.createFile("New File.txt");
        fileSystem.writeToFile("New File.txt", 1, "Hello World lq");
        fileSystem.writeToFile("New File.txt", 1, "Java");

        assertEquals(newContent, fileSystem.getCurrentFolder().getFiles().get("New File.txt").getContent().get(0));

    }

    @Test
    public void When_WriteEmptyStringInEmptyLineFile_Expect_HardDriveOutOfStorageExceptionToBeThrown()
            throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "");
        assertEquals("", fileSystem.getCurrentFolder().getFiles().get("File.txt").getContent().get(0));

    }

    @Test
    public void When_CallDisplayContentOnExistingFile_Expect_ToReturnFileContent() throws IOException {
        fileSystem.createFile("New File.txt");
        fileSystem.writeToFile("New File.txt", 1, "Java");
        assertEquals("1:  Java", fileSystem.displayFileContent("New File.txt"));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_CallDisplayContentNonExistingFile_Expect_UnexistingFileExceptionToBeThrown() throws IOException {
        assertEquals("0", fileSystem.displayFileContent("New File.txt"));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_RemoveFileWhichDoesNotExist_Expect_AnUnexistingFileExceptionToBeThrown() throws IOException {
        fileSystem.remove("File.txt");
    }

    @Test
    public void When_RemoveFileWhichExists_Expect_RemoveMethodToReturnTrue() throws IOException {
        fileSystem.createFile("File.txt");
        assertTrue(fileSystem.remove("File.txt"));
    }

    @Test
    public void When_RemoveLinesFromFileWhichExistsAndLineIndexesContainText_Expect_RemoveMethodToReturnTrue()
            throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "abc");
        fileSystem.writeToFile("File.txt", 2, "abc");
        assertTrue(fileSystem.removeLines("File.txt", 1, 2));
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_RemoveLinesFromFileWhichExistsAndLineIndexesDoNotContainText_Expect_RemoveMethodToThrowAFileInvalidLineException()
            throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 2, "abc");
        fileSystem.writeToFile("File.txt", 3, "abc");
        assertTrue(fileSystem.removeLines("File.txt", 1, 2));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_RemoveLinesFromFileWhichDoesNotExist_Expect_RemoveMethodToThrowAnUnexistingFileException()
            throws IOException {
        fileSystem.writeToFile("File.txt", 2, "abc");
        fileSystem.writeToFile("File.txt", 3, "abc");
        assertTrue(fileSystem.removeLines("File.txt", 1, 2));
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_RemoveLinesFromFileWhichExistsButFromIndexIsGreaterThanTo_Expect_RemoveMethodToThrowAFileInvalidLineException()
            throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 2, "abc");
        fileSystem.writeToFile("File.txt", 3, "abc");
        assertTrue(fileSystem.removeLines("File.txt", 20, 2));
    }

    @Test(expected = FileInvalidLineException.class)
    public void When_RemoveLinesFromFileWhichExistsButIndexesAreNegative_Expect_RemoveMethodToThrowAnUnexistingFileException()
            throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "abc");
        fileSystem.writeToFile("File.txt", 2, "abc");
        assertTrue(fileSystem.removeLines("File.txt", -1, -2));
    }

    @Test
    public void When_CountWordsOfFileContent_Expect_CountWordsMethodToReturnCorrectWordsCount() throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "abc bc");
        fileSystem.writeToFile("File.txt", 2, "abc bc");
        assertEquals(4, fileSystem.getFileWordsCount("File.txt"));
    }

    @Test
    public void When_CountWordsOfEmptyFile_Expect_CountWordsMethodToReturnZero() throws IOException {
        fileSystem.createFile("File.txt");
        assertEquals(0, fileSystem.getFileWordsCount("File.txt"));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_CountWordsOfNonExistingFile_Expect_CountWordsMethodToReturnZero() throws IOException {

        assertEquals(0, fileSystem.getFileWordsCount("File.txt"));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_CountLinesOfNonExistingFileName_Expect_CountLinesMethodToThrowAnUnexistingFileException()
            throws IOException {

        assertEquals(1, fileSystem.getFileLinesCount("File.txt"));
    }

    @Test
    public void When_CountLinesOfExistingFileName_Expect_CountLinesMethodToReturnLinesCount() throws IOException {
        fileSystem.createFile("File.txt");
        fileSystem.writeToFile("File.txt", 1, "line1");
        fileSystem.writeToFile("File.txt", 2, "line2");
        fileSystem.writeToFile("File.txt", 3, "line3");
        assertEquals(3, fileSystem.getFileLinesCount("File.txt"));
    }

}
