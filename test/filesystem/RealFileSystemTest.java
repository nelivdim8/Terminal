package filesystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import exceptions.FileInvalidLineException;
import exceptions.UnexistingFileException;

public class RealFileSystemTest {
    private FileSystem fileSystem;
    private Path root;
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setData() {
        fileSystem = new RealFileSystem();
        root = Paths.get("D:\\");

    }

    public String changeLineEncoding(String text, Charset oldCharset, Charset newCharset) {
        byte bytes[] = text.getBytes(oldCharset);
        return new String(bytes, newCharset);
    }

    @Test
    public void When_WeNotChangedDirectory_Expect_GetCurrentFolderNameToReturnRoot() throws IOException {

        assertEquals(fileSystem.getCurrentFolderName(), "D:\\");
    }

    @Test
    public void When_DirectoryIsChangedToExistingSubdirectory_Expect_GetCurrentFolderNameToReturnCurrentFolder()
            throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        assertEquals(root.resolve(name).toString(), fileSystem.getCurrentFolderName());

    }

    @Test
    public void When_DirectoryIsChangedToExistingSubdirectory_Expect_ChandeDirToReturnTrue() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        assertTrue(fileSystem.changeDir(name));

    }

    @Test
    public void When_DirectoryIsChangedToUnExistingSubdirectory_Expect_GetCurrentFolderNameToReturnDifferentFromExpectedFolder()
            throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = "newFolder";
        fileSystem.changeDir(name);
        assertNotEquals(root.resolve(name).toString(), fileSystem.getCurrentFolderName());

    }

    @Test
    public void When_DirectoryIsChangedToUnExistingSubdirectory_Expect_ChangeDirToReturnFalse() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = "newFolder";
        assertFalse(fileSystem.changeDir(name));

    }

    @Test
    public void When_DirectoryIsChangedToUnExistingSubdirectoryAndThenToParent_Expect_GetCurrentFolderNameToReturnCurrentFolder()
            throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        fileSystem.changeDir("..");
        assertEquals(root.toString(), fileSystem.getCurrentFolderName());

    }

    @Test
    public void When_DirectoryIsChangedToUnExistingSubdirectoryAndThenToParent_Expect_ChangeDirToReturnTrue()
            throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        assertTrue(fileSystem.changeDir(".."));

    }

    @Test
    public void When_DirectoryIsChangedToSameDirectory_Expect_ChandeDirToReturnTrue() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        assertTrue(fileSystem.changeDir("."));

    }

    @Test
    public void When_LocalDiskIsChangedToExistingOne_Expect_GetCurrentFolderNameToReturnTheNewLocalDisk()
            throws IOException {

        fileSystem.changeDir("C:");
        assertEquals("C:\\", fileSystem.getCurrentFolderName());

    }

    @Test
    public void When_LocalDiskIsChangedToUnExistingOne_Expect_GetCurrentFolderNameToReturnTheOriginalLocalDisk()
            throws IOException {

        fileSystem.changeDir("N:");
        assertEquals(root.toString(), fileSystem.getCurrentFolderName());

    }

    @Test
    public void When_CurrentFolderContainsFile_Expect_CurrentFolderContainsFileToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();

        assertTrue(fileSystem.currentFolderContainsFile(name));
    }

    @Test
    public void When_CurrentFolderDoesNotContainFile_Expect_CurrentFolderContainsFileToReturnFalse()
            throws IOException {

        assertFalse(fileSystem.currentFolderContainsFile("newfiletemp.txt"));
    }

    @Test
    public void When_FileExistsAndLineIsValid_Expect_WriteToFileStringIntStringToReturnTrue() throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.writeToFile(name, 0, "Това е само тест!"));
    }

    @Test
    public void When_FileExistsAndLineIsValid_Expect_AfterExecutingWriteToFileStringIntStringTextToBeWrittenOnCurrentLine()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd = "Това е само тест!";
        List<String> oldContent = Files.readAllLines(root.resolve(name));
        fileSystem.writeToFile(name, 0, textToAdd);
        List<String> newContent = Files.readAllLines(root.resolve(name));
        assertTrue(oldContent.isEmpty());
        assertTrue(newContent.get(0).equals(textToAdd));

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_FileExistsAndLineIsLessThanZero_Expect_WriteToFileStringIntStringToThrowAnException()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd = "Това е само тест!";
        List<String> oldContent = Files.readAllLines(root.resolve(name));
        fileSystem.writeToFile(name, -1, textToAdd);

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_FileExistsAndLineIndexIsGreaterOrEqualsToFileLines_Expect_WriteToFileStringIntStringToThrowAnException()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd = "Това е само тест!";
        List<String> oldContent = Files.readAllLines(root.resolve(name));
        fileSystem.writeToFile(name, 1, textToAdd);

    }

    @Test(expected = UnexistingFileException.class)
    public void When_FileDoesNotExist_Expect_WriteToFileStringIntStringToThrowAnUexistingFileException()
            throws IOException {

        String name = "temp.txt";
        String textToAdd = "Това е само тест!";
        fileSystem.writeToFile(name, 0, textToAdd);

    }

    @Test
    public void When_FileExistsAndLineIsValidAndContainsText_Expect_AfterExecutingWriteToFileStringIntStringTextToBeRewritten()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd = "Това е само тест!";
        String newTextToAdd = "Това е нов тест!";

        fileSystem.writeToFile(name, 0, textToAdd);
        List<String> oldContent = Files.readAllLines(root.resolve(name));
        fileSystem.writeToFile(name, 0, newTextToAdd);

        List<String> newContent = Files.readAllLines(root.resolve(name));
        assertTrue(oldContent.get(0).equals(textToAdd));
        assertEquals(1, oldContent.size());
        assertTrue(newContent.get(0).equals(newTextToAdd));
        assertEquals(1, newContent.size());

    }

    @Test
    public void When_FileExistsAndLineIsValidButNewCharset_Expect_WriteToFileStringIntStringCharsetToThrowAnUnmappableCharacterException()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.writeToFile(name, 0, "Това е само тест!", Charset.forName("US-ASCII")));
    }

    @Test
    public void When_FileExistsAndLineIsValid_Expect_WriteToFileStringIntStringCharsetToReturnTrue()
            throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.writeToFile(name, 0, "Това е само тест!", Charset.forName("UTF-16")));
    }

    @Test
    public void When_TryToWriteContentInCompatibleEncoding_Expect_WriteToFileStringIntStringCharsetToReturnTrue()
            throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String content = "Това е тест";
        fileSystem.writeToFile(name, 0, content, StandardCharsets.ISO_8859_1);

    }

    @Test
    public void When_FileContainsText_ExpecttestDisplayFileContentStringToReturnTheContent() throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        fileSystem.writeToFile(name, 0, "Hello");
        fileSystem.writeToFile(name, 1, "World");
        String content = fileSystem.displayFileContent(name);
        assertEquals("Hello\nWorld\n", content);

    }

    @Test
    public void When_FileIsEmpty_ExpecttestDisplayFileContentStringToReturnEmptyString() throws IOException {

        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String content = fileSystem.displayFileContent(name);
        assertEquals("", content);

    }

    @Test(expected = UnexistingFileException.class)
    public void When_FileIsEmpty_ExpecttestDisplayFileContentStringAnToThrowAnException() throws IOException {

        String content = fileSystem.displayFileContent("temp.txt");
        assertEquals("", content);

    }

    @Test
    public void When_EncodingIsDefaultAndInputIsCompatible_ChangeDisplayFileContentStringCharsetToReturnCorectlyEncodedContent()
            throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String content = "Това е тест";
        fileSystem.writeToFile(name, 0, content);
        fileSystem.displayFileContent(name, StandardCharsets.ISO_8859_1);
        assertEquals(changeLineEncoding(content, StandardCharsets.UTF_8, StandardCharsets.ISO_8859_1) + "\n",
                fileSystem.displayFileContent(name, StandardCharsets.ISO_8859_1));

    }

    @Test
    public void When_EncodingIsDefaultAndInputIsIncompatible_ChangeDisplayFileContentStringCharsetToThrowAnException()
            throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String content = "Това е тест";
        fileSystem.writeToFile(name, 0, content);
        assertEquals(changeLineEncoding(content, StandardCharsets.UTF_8, StandardCharsets.US_ASCII) + "\n",
                fileSystem.displayFileContent(name, StandardCharsets.US_ASCII));

    }

    @Test
    public void When_CreateFileWithNameWhichDoesNotYetExistInCurrentFolder_Expect_CreateFileToReturnTrue()
            throws IOException {
        assertTrue(fileSystem.createFile("filename.txt"));
        Files.delete(root.resolve("filename.txt"));

    }

    @Test
    public void When_CreateFileWithNameWhichDoesNotYetExistInCurrentFolder_Expect_FileToExist() throws IOException {
        fileSystem.createFile("filename.txt");
        assertTrue(Files.exists(root.resolve("filename.txt")));
        Files.delete(root.resolve("filename.txt"));
    }

    @Test
    public void When_CreateFileWithNameWhichAlreasyExistsInCurrentFolder_Expect_CreateFileToReturnFalse()
            throws IOException {
        fileSystem.createFile("filename.txt");
        assertFalse(fileSystem.createFile("filename.txt"));
        Files.delete(root.resolve("filename.txt"));
    }

    @Test
    public void When_CreateFileWithNameWhichAlreadyExiststInCurrentFolder_Expect_OriginalFileToExist()
            throws IOException {
        fileSystem.createFile("filename.txt");
        fileSystem.createFile("filename.txt");
        assertTrue(Files.exists(root.resolve("filename.txt")));
        Files.delete(root.resolve("filename.txt"));
    }

    @Test
    public void When_TryToMakeDirWhichNotYetExitst_Expect_MkDirToReturnTrue() throws IOException {
        assertTrue(fileSystem.createFile("tempfolder"));
        Files.delete(root.resolve("tempfolder"));
    }

    @Test
    public void When_TryToMakeDirWhichNotYetExitst_Expect_AfterExecutingMkDirFolderToExist() throws IOException {
        fileSystem.createFile("tempfolder");
        assertTrue(Files.exists(root.resolve("tempfolder")));
        Files.delete(root.resolve("tempfolder"));
    }

    @Test
    public void When_RemoveValidLinesFromMiddle_Expect_ToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();

        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        String textToAdd2 = "Text line index 2";
        String textToAdd3 = "Text line index 3";

        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        fileSystem.writeToFile(name, 2, textToAdd2);
        fileSystem.writeToFile(name, 3, textToAdd3);
        assertTrue(fileSystem.removeLines(name, 1, 2));

    }

    @Test
    public void When_RemoveValidLines_Expect_ToBeRemovedCorrectLines() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();

        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        String textToAdd2 = "Text line index 2";
        String textToAdd3 = "Text line index 3";
        List<String> expected = Arrays.asList(textToAdd0, textToAdd3);

        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        fileSystem.writeToFile(name, 2, textToAdd2);
        fileSystem.writeToFile(name, 3, textToAdd3);
        fileSystem.removeLines(name, 1, 2);
        List<String> content = Files.readAllLines(root.resolve(name));
        assertEquals(expected, content);

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_StartingLineIsILessThanZero_Expect_RemoveLinesToThrowAFileInvalidLineException()
            throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        fileSystem.removeLines(name, -1, 0);

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_StartingLineIsGreaterThanEnding_Expect_RemoveLinesToThrowAFileInvalidLineException()
            throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        fileSystem.removeLines(name, 1, 0);

    }

    @Test
    public void When_StartingLineIsValidAndEqualToEnding_Expect_ToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        assertTrue(fileSystem.removeLines(name, 1, 1));

    }

    @Test
    public void When_StartingLineIsValidAndEqualToEnding_Expect_ToRemoveThisLineFromFileContent() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String textToAdd0 = "Text line index 0";
        String textToAdd1 = "Text line index 1";
        List<String> expected = Arrays.asList(textToAdd0);
        fileSystem.writeToFile(name, 0, textToAdd0);
        fileSystem.writeToFile(name, 1, textToAdd1);
        fileSystem.removeLines(name, 1, 1);
        List<String> content = Files.readAllLines(root.resolve(name));
        assertEquals(expected, content);

    }

    @Test
    public void When_WantToRemoveWhichExists_Expect_RemoveToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.remove(name));

    }

    @Test(expected = UnexistingFileException.class)
    public void When_WantToRemoveFileWhichDoesNotExist_Expect_RemoveToThrowUnexistingFileException()
            throws IOException {

        assertFalse(fileSystem.remove("temp.txt"));

    }

    @Test
    public void When_WantToRemoveWhichExists_Expect_AfterRemoveExecutionFileNotToExist() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        fileSystem.remove(name);
        assertFalse(Files.exists(root.resolve(name)));

    }

    @Test
    public void When_FileIsEmpty_Expect_GetFileWordsCountToReturnZero() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertEquals(0, fileSystem.getFileWordsCount(name));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_FileDoesNotExist_Expect_GetFileWordsCountToThrowAnUnexistingFileException() throws IOException {
        assertEquals(0, fileSystem.getFileWordsCount("tempfile.txt"));
    }

    @Test
    public void When_FileIsNotEmpty_Expect_GetFileWordsCountToReturnCurrentWordsCount() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        fileSystem.writeToFile(name, 0, "this is a text");
        fileSystem.writeToFile(name, 1, "second line text");
        assertEquals(7, fileSystem.getFileWordsCount(name));
    }

    @Test
    public void When_FileIsEmpty_Expect_GetFileLinesCountToReturnZero() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertEquals(0, fileSystem.getFileLinesCount(name));
    }

    @Test(expected = UnexistingFileException.class)
    public void When_FileDoesNotExist_Expect_GetFileLinesCountToThrowAnUnexistingFileException() throws IOException {
        assertEquals(0, fileSystem.getFileLinesCount("tempfile.txt"));
    }

    @Test
    public void When_FileIsNotEmpty_Expect_GetFileLinesCountToReturnCurrentLinesCount() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        String name = tempFile.getFileName().toString();
        fileSystem.writeToFile(name, 0, "text");
        fileSystem.writeToFile(name, 1, "text");
        fileSystem.writeToFile(name, 2, "text");
        fileSystem.writeToFile(name, 3, "text");

        assertEquals(4, fileSystem.getFileLinesCount(name));
    }

    @Test
    public void When_FolderIsEmpty_Expect_ListSortedFilesToReturnEmptyString() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        assertEquals("", fileSystem.listSortedFiles());
    }

    @Test
    public void When_FolderIsEmpty_Expect_ListFilesToReturnEmptyString() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        assertEquals("", fileSystem.listFiles());
    }

    @Test
    public void When_FolderIsEmpty_Expect_ListFilesToReturFileInsideCurrentFolder() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        Path tempFile1 = Files.createTempFile(tempFolder, "temp1", ".txt");
        String file1 = tempFile1.getFileName().toString();
        tempFile1.toFile().deleteOnExit();
        Path tempFile2 = Files.createTempFile(tempFolder, "temp2", ".txt");
        String file2 = tempFile2.getFileName().toString();
        tempFile2.toFile().deleteOnExit();
        Path tempFile3 = Files.createTempFile(tempFolder, "temp3", ".txt");
        String file3 = tempFile3.getFileName().toString();
        tempFile3.toFile().deleteOnExit();
        String result = file1 + "\n" + file2 + "\n" + file3 + "\n";
        assertEquals(result, fileSystem.listFiles());
    }

    @Test
    public void When_FolderIsEmpty_Expect_ListSortedFilesToReturFileInsideCurrentFoldeSortedBySizer()
            throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        fileSystem.changeDir(name);
        Path tempFile1 = Files.createTempFile(tempFolder, "temp1", ".txt");
        String file1 = tempFile1.getFileName().toString();
        fileSystem.writeToFile(file1, 0, "Hello");
        tempFile1.toFile().deleteOnExit();
        Path tempFile2 = Files.createTempFile(tempFolder, "temp2", ".txt");
        String file2 = tempFile2.getFileName().toString();
        tempFile2.toFile().deleteOnExit();
        Path tempFile3 = Files.createTempFile(tempFolder, "temp3", ".txt");
        String file3 = tempFile3.getFileName().toString();
        fileSystem.writeToFile(file3, 0, "Hello World");
        tempFile3.toFile().deleteOnExit();
        String result = file3 + "\n" + file1 + "\n" + file2 + "\n";
        assertEquals(result, fileSystem.listSortedFiles());
    }

    @Test
    public void When_InputIsFolderName_ExpectIsAFileNameToReturnFalse() throws IOException {
        Path tempFolder = Files.createTempDirectory(root, "newFolder");
        tempFolder.toFile().deleteOnExit();
        String name = tempFolder.getFileName().toString();
        assertFalse(fileSystem.isAFileName(name));
    }

    @Test
    public void When_InputIsUnexistingFolderName_ExpectIsAFileNameToReturnFalse() throws IOException {

        assertFalse(fileSystem.isAFileName("newFolder"));
    }

    @Test
    public void When_InputIsFileName_ExpectIsAFileNameToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.isAFileName(name));
    }

    @Test
    public void When_InputIsUnexistingFileName_ExpectIsAFileNameToReturnFalse() throws IOException {

        assertFalse(fileSystem.isAFileName("tempfile.txt"));
    }

    @Test
    public void When_TryToSetEncodingTheSameToCurrentOne_Expect_ToReturnFalse() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertFalse(fileSystem.setFileEncoding(name, StandardCharsets.UTF_8));
    }

    @Test
    public void When_TryToSetEncodingDifferentFromCurrentOne_Expect_ToReturnTrue() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        assertTrue(fileSystem.setFileEncoding(name, StandardCharsets.ISO_8859_1));
    }

    @Test
    public void When_TryToSetEncodingTheSameToCurrentOne_Expect_ContentEncodingNotToBeChanged() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String text = "Здравей!";
        fileSystem.writeToFile(name, 0, text);
        fileSystem.setFileEncoding(name, StandardCharsets.UTF_8);
        assertEquals(changeLineEncoding(text, StandardCharsets.UTF_8, StandardCharsets.UTF_8) + "\n",
                fileSystem.displayFileContent(name));

    }

    @Test
    public void When_TryToSetEncodingDifferentFromCurrentOne_Expect_TontentEncodingToBeChanged() throws IOException {
        Path tempFile = Files.createTempFile(root, "temp", ".txt");
        tempFile.toFile().deleteOnExit();
        String name = tempFile.getFileName().toString();
        String text = "Здравей!";
        fileSystem.writeToFile(name, 0, text);
        fileSystem.setFileEncoding(name, StandardCharsets.UTF_16BE);
        String result = changeLineEncoding(text, StandardCharsets.UTF_8, StandardCharsets.UTF_16BE) + "\n";
        System.out.println(result);
        fileSystem.displayFileContent(name);
        System.out.println();
        assertEquals(result, fileSystem.displayFileContent(name));
    }

}
