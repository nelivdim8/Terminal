package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.VirtualFileSystem;

public class MakeDirCommandTest {
    private Command command;
    private VirtualFileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_TryToMakeDirectoryWithDuplicatedName_Expect_mkDirToReturnFalse() throws IOException {
        command = cmdFactory.getCommand("mkdir NewFolder", fileSystem);
        command.executeCommand();
        assertFalse(command.executeCommand());
    }

    @Test
    public void When_TryToMakeDirectoryWithSameNameAsParent_Expect_mkDirToReturnFalse() throws IOException {
        command = cmdFactory.getCommand("mkdir home", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_TryToMakeDirectoryWithNoDuplicatedName_Expect_mkDirToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("mkdir Unique", fileSystem);
        assertTrue(command.executeCommand());
    }

    @Test
    public void When_MakeDirectoryInRootDrectory_Expect_ParentOfSubdirectoryToBeRootAndRootToContainsNewDirectoryIntsList()
            throws IOException {
        command = cmdFactory.getCommand("mkdir Unique", fileSystem);
        command.executeCommand();
        assertTrue(fileSystem.getCurrentFolder().getSubfolders().containsKey("Unique"));
        assertEquals(fileSystem.getCurrentFolder(),
                fileSystem.getCurrentFolder().getSubfolders().get("Unique").getParent());
    }

    @Test
    public void When_MakeDirectoryInSubdirectoryOfRoot_Expect_CorrectHierarchyOfFolder() throws IOException {
        command = cmdFactory.getCommand("mkdir Unique", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cd Unique", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("mkdir Subdirectory", fileSystem);
        command.executeCommand();

        assertTrue(fileSystem.getCurrentFolder().getName().equals("Unique"));
        assertTrue(fileSystem.getCurrentFolder().getParent().getName().equals("home"));
        assertNotNull(fileSystem.getCurrentFolder().getSubfolders().get("Subdirectory"));

    }
}
