package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.VirtualFileSystem;

public class PipeCommandTest {
    private Command command;
    private VirtualFileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_FirstCommandIsLsAndSecondWcLinesAndThereAreTwoFilesInCurrentDirectory_Expect_ExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File2.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("ls | wc -l", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_FirstCommandIsLsAndSecondWcLinesAndThereAreTwoFilesInCurrentDirectory_Expect_OutputToBeEqualToTwo()
            throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File2.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("ls | wc -l", fileSystem);
        command.executeCommand();
        assertEquals("2", command.getOutput());

    }

    @Test
    public void When_FirstCommandIsLsAndSecondWcAndThereAreTwoFilesInCurrentDirectory_Expect_OutputToBeEqualToTwo()
            throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File2.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("ls | wc", fileSystem);
        command.executeCommand();
        assertEquals("2", command.getOutput());

    }

    @Test
    public void When_FirstCommandIsLsSortedAndSecondWcAndThereAreTwoFilesInCurrentDirectory_Expect_OutputToBeEqualToTwo()
            throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File2.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("ls --sorted desc | wc", fileSystem);
        command.executeCommand();
        assertEquals("2", command.getOutput());

    }

    @Test
    public void When_FirstCommandIsLsSortedAndSecondWcLinesAndThereAreTwoFilesInCurrentDirectory_Expect_OutputToBeEqualToTwo()
            throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File2.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("ls --sorted desc | wc -l", fileSystem);
        command.executeCommand();
        assertEquals("2", command.getOutput());

    }

    @Test
    public void When_FirstCommandIsWcAndSecondIsLs_Expect_SecondCommandToBeInvalid() throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File1.txt | ls", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_FirstCommandIsWcLinesAndSecondIsLs_Expect_SecondCommandToBeInvalid() throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File1.txt | ls", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_FirstCommandIsWcAndSecondIsMkdir_Expect_ADirectoryWithOutputNameToBeCreated() throws IOException {
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File1.txt | mkdir", fileSystem);
        command.executeCommand();
        assertTrue(fileSystem.getCurrentFolder().containsSubFolder("0"));

    }

    @Test
    public void When_FirstCommandIsWcAndSecondIsCd_Expect_CurrentDirectoryToBeChangedWithGivenOutput()
            throws IOException {
        command = cmdFactory.getCommand("mkdir 0", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File1.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File1.txt | cd", fileSystem);
        command.executeCommand();
        assertTrue(fileSystem.getCurrentFolder().getName().equals("0"));

    }

}
