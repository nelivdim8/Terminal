package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.VirtualFileSystem;

public class CreateFileCommandTest {

    private Command command;
    private VirtualFileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        cmdFactory = CommandFactory.getInstance();
        fileSystem = new VirtualFileSystem();

    }

    @Test
    public void When_CreateFileInRootDirectory_Expect_createFileToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CreateFileWithAlreadyExistingNameInRootDirectory_Expect_createFileToReturnFalse()
            throws IOException {
        command = cmdFactory.getCommand("create_file Unique", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file Unique", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_CreateFileInRootDirectory_Expect_correctHierarchy() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        assertTrue(fileSystem.currentFolderContainsFile("File.txt"));

    }

    @Test
    public void When_CreateFileInSubdirectoryOfRootDirectory_Expect_correctHierarchy() throws IOException {
        command = cmdFactory.getCommand("mkdir Subfolder", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cd Subfolder", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        assertTrue(fileSystem.currentFolderContainsFile("File.txt"));
    }

    @Test
    public void When_CreateFileInSubdirectoryOfRootDirectory_Expect_FileSizeToBeZero() throws IOException {
        command = cmdFactory.getCommand("mkdir Subfolder", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cd Subfolder", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();

        assertEquals(0, fileSystem.getCurrentFolderFile("File.txt").getSize());

    }
}
