package command;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class CommandFactoryTest {

    private CommandFactory cmdFactory;
    private FileSystem fileSystem;
    private Command command;

    @Before
    public void setData() {
        cmdFactory = CommandFactory.getInstance();
        fileSystem = new VirtualFileSystem();
    }

    @Test
    public void When_InputMkDirCommandContainsSignleWord_Expect_ToReturnMakeDirCommandInstance() {
        command = cmdFactory.getCommand("mkdir new", fileSystem);
        assertTrue(command instanceof MakeDirCommand);

    }

    @Test
    public void When_InputMkDirCommandContainsOnlyCommandAndSpace_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("mkdir ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputMkDirCommandContainsCommandAndInvalidCharactesAfter_Expect_ToReturnNullInstance() {
        command = cmdFactory.getCommand("mkdir *&^%$", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputMkDirCommandContainsWordsSeparatedBySpace_Expect_ToReturnNullInstance() {
        command = cmdFactory.getCommand("mkdir new folder", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputContainsNoLettersAfterMkDirCommand_Expect_ToReturnNullInstance() {
        command = cmdFactory.getCommand("mkdir ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputContainsInValidSymbolsAfterMkDirCommand_Expect_ToReturnNullInstance() {
        command = cmdFactory.getCommand("mkdir *&^%$", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCdCommandContainsWordsSeparatedBySpace_Expect_ToReturnCdCommandInstance() {

        command = cmdFactory.getCommand("cd folder", fileSystem);
        assertTrue(command instanceof CdCommand);

    }

    @Test
    public void When_InputCdCommandContainsOnlyCommandAndSpace_Expect_ToReturnNullInstance() {

        command = cmdFactory.getCommand("cd ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCdContainsCommandAndDot_Expect_ToReturnCdCommandInstance() {
        command = cmdFactory.getCommand("cd .", fileSystem);
        assertTrue(command instanceof CdCommand);

    }

    @Test
    public void When_InputCdContainsCommandAndTwoDots_Expect_ToReturnCdCommandInstance() {
        command = cmdFactory.getCommand("cd ..", fileSystem);
        assertTrue(command instanceof CdCommand);

    }

    @Test
    public void When_InputCdCommandContainsCommandAndInvalidCharactesAfter_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("cd *&^%$", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCreateFileCommandContainsWord_Expect_ToReturnCreateFileCommandInstance() {
        command = cmdFactory.getCommand("create_file file", fileSystem);
        assertTrue(command instanceof CreateFileCommand);

    }

    @Test
    public void When_InputCreateFileCommandContainsOnlyCommandAndSpace_Expect_ToReturnNullCommandInstance() {

        command = cmdFactory.getCommand("create_file ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCreateFileCommandContainsCommandAndInvalidCharactesAfter_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("create_file *&^%$", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCatCommandContainsWordsSeparatedBySpace_Expect_ToReturnCatCommandInstance() {

        command = cmdFactory.getCommand("cat file", fileSystem);
        assertTrue(command instanceof CatCommand);

    }

    @Test
    public void When_InputCatCommandContainsOnlyCommandAndSpace_Expect_ToReturnNullCommandInstance() {

        command = cmdFactory.getCommand("cat ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputCatCommandContainsCommandAndInvalidCharactesAfter_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("cat *&^%$", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputWriteCommandContainsWordsSeparatedBySpaceFollowedByDigitsAndAllTypeSymbolsAtEnd_Expect_ToReturnWriteCommandInstance() {
        command = cmdFactory.getCommand("write file 2 hello 847 &%$GN.", fileSystem);
        assertTrue(command instanceof WriteCommand);

    }

    @Test
    public void When_InputWriteCommandContainsOnlyCommandAndSpace_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("write ", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputWriteCommandContainsAllTypesOfSymbolsFollowedByDigitsAndAllTypeSymbolsAtEnd_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("write *&^%$ 2 Hello!@)", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputWriteCommandContainsAllTypesOfSymbolsFollowedByLettersAndAllTypeSymbolsAtEnd_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("write *&^%$ abc Hello!@)", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputWriteCommandContainsAllTypesOfSymbols_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("write *&^%$ ab&^c Hello!@)", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputWriteCommandContainsOnlyDigits_Expect_ToReturnWriteCommandInstance() {
        command = cmdFactory.getCommand("write 2 2 3", fileSystem);
        assertTrue(command instanceof WriteCommand);

    }

    @Test
    public void When_InputWriteCommandContainsOnlyLetters_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("write a b c", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputLsIsEntered_Expect_ToReturnLsCommandInstance() {
        command = cmdFactory.getCommand("ls", fileSystem);
        assertTrue(command instanceof LsCommand);

    }

    @Test
    public void When_InputLsIsEnteredWithSpaceNameOfCurrentDir_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("ls - folder", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputLsIsEnteredWithNoSpaceAndNameOfCurrentDir_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("ls -folder", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputLsSortedIsEntered_Expect_ToReturnLsSortedDescCommandInstance() {
        command = cmdFactory.getCommand("ls --sorted desc", fileSystem);
        assertTrue(command instanceof LsSortedDescCommand);

    }

    @Test
    public void When_InputLsSortedIsEnteredWithOneDash_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("ls -sorted desc", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputLsSortedIsEnteredWithCurrentDirName_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("ls --sorted desc folder", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

    @Test
    public void When_InputLsSortedIsEnteredWithMissingWord_Expect_ToReturnNullCommandInstance() {
        command = cmdFactory.getCommand("ls --sorted", fileSystem);
        assertTrue(command instanceof NullCommand);

    }

}
