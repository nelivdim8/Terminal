package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class CdCommandTest {
    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_ChangeDirectory_Expect_GetCurrentFolderNameToReturnCorrectPath() throws IOException {
        String path = "/home/NewFolder/";
        command = cmdFactory.getCommand("mkdir NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd NewFolder", fileSystem);
        command.executeCommand();
        assertEquals(path, fileSystem.getCurrentFolderName());
    }

    @Test
    public void When_ReturnToPreviosDirectory_Expect_GetCurrentFolderNameToReturnCorrectPath() throws IOException {
        String path = "/home/";
        command = cmdFactory.getCommand("mkdir NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd ..", fileSystem);
        command.executeCommand();
        assertEquals(path, fileSystem.getCurrentFolderName());
    }

    @Test
    public void When_CurrentIsRootAndWantToReturnToPreviosDirectory_Expect_GetCurrentFolderNameToRemainTheSame()
            throws IOException {
        String path = "/";

        command = cmdFactory.getCommand("cd ..", fileSystem);
        command.executeCommand();

        assertEquals(path, fileSystem.getCurrentFolderName());
    }

    @Test
    public void When_UseCommandToGoToThisDirectory_Expect_GetCurrentFolderNameToRemainTheSame() throws IOException {
        String path = "/home/NewFolder/";
        command = cmdFactory.getCommand("mkdir NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd .", fileSystem);
        command.executeCommand();
        assertEquals(path, fileSystem.getCurrentFolderName());
    }

    @Test
    public void When_ChangeDirectoryToExistingSubdirectory_Expect_executeCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("mkdir NewFolder", fileSystem);
        command.executeCommand();

        command = cmdFactory.getCommand("cd NewFolder", fileSystem);

        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CurrentDirectoryIsRootAndTryToChangeItToParnt_Expect_executeCommandToReturnFalse()
            throws IOException {

        command = cmdFactory.getCommand("cd ..", fileSystem);
        command.executeCommand();
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_ChangeDirectoryToNonExistingSubdirectory_Expect_executeCommandToReturnFalse() throws IOException {
        command = cmdFactory.getCommand("cd Unique", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_ChangeRootDirectoryToParent_Expect_executeCommandToReturnFalse() throws IOException {
        command = cmdFactory.getCommand("cd ..", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_ChangeRootDirectoryToCurrent_Expect_executeCommandToReturnTrue() throws IOException {

        command = cmdFactory.getCommand("cd ..", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_ChangeSubdirectoryOfRootDirectoryToCurrent_Expect_executeCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("mkdir Unique", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cd .", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_ChangeSubdirectoryOfRootDirectoryToParent_Expect_executeCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("mkdir Unique", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cd ..", fileSystem);
        assertTrue(command.executeCommand());

    }

}
