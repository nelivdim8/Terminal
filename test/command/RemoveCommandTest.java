package command;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class RemoveCommandTest {
    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_RemoveLinesFromFileWhichExistsButIsEmpty_Expect_removeCommandExecuteCommandToReturnFalse()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("remove File.txt 1-3", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_RemoveLinesFromNonEmptyFileWhichExistsButHaveLessFilledLinesThanWantToRemove_Expect_removeCommandExecuteCommandToReturnFalse()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 3 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("remove File.txt 1-3", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_RemoveLinesFromNonEmptyFileWhichExistsButHaveCountOfLinesThanWantToRemoveButDifferentIndexes_Expect_removeCommandExecuteCommandToReturnFalse()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 3 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 4 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("remove File.txt 1-3", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_RemoveLinesFromNonEmptyFileWhichExistsAndIndexesToRemoveAreValid_Expect_removeCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 3 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("remove File.txt 1-3", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_RemoveLinesOfUnexistingFile_Expect_RemoveCommandExecuteMethodToReturnFalse() throws IOException {

        command = cmdFactory.getCommand("write File.txt 1 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 3 hi", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("remove File.txt 1-3", fileSystem);
        assertFalse(command.executeCommand());

    }

}
