package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class WcCommandTest {
    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_CountWordsOfExistingInputFileName_Expect_WcCommandExecuteCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two three", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two three", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountWordsOfExistingInputFileName_Expect_WcCommandToReturnWordsCountInFileContent()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two three", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File.txt", fileSystem);
        command.executeCommand();
        assertEquals("3", command.getOutput());

    }

    @Test
    public void When_CountWordsOfExistingInputFileNameInWhichisWrittenTwice_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two three", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 one two three", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountWordsOfExistingInputFileNameInWhichisWrittenTwice_Expect_WcCommandOutputToReturnWordsCountInFileContent()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 one two", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File.txt", fileSystem);
        command.executeCommand();
        assertEquals("4", command.getOutput());

    }

    @Test
    public void When_CountWordsOfUnexistingInputFileName_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("wc File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountWordsOfUnexistingInputFileName_Expect_WcCommandToLookAtItAsTextAndReturnItsWordsCount()
            throws IOException {
        command = cmdFactory.getCommand("wc File.txt", fileSystem);
        command.executeCommand();
        assertEquals("1", command.getOutput());

    }

    @Test
    public void When_CountWordsOfInputWithMoreThanOneWord_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File.txt is a looked as a text", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountWordsOfInputWithMoreThanOneWord_Expect_WcCommandToLookAtItAsTextAndReturnItsWordsCount()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc File.txt is a looked as a text", fileSystem);
        command.executeCommand();
        assertEquals("7", command.getOutput());

    }

    @Test
    public void When_CountWordsOfInputWithOneWordWithInvalidForAFileSymbols_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("wc *^%((", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountWordsOfInputWithOneWordWithInvalidForAFileSymbols_Expect_WcCommandToLookAtItAsTextAndReturnItsWordsCount()
            throws IOException {
        command = cmdFactory.getCommand("wc *^%((", fileSystem);
        command.executeCommand();
        assertEquals("1", command.getOutput());

    }

    @Test
    public void When_CountWordsOfInputWithManyWords_Expect_WcCommandToLookAtItAsTextAndReturnItsWordsCount()
            throws IOException {
        command = cmdFactory.getCommand("wc this is just a text", fileSystem);
        command.executeCommand();
        assertEquals("5", command.getOutput());

    }

    @Test
    public void When_CountWordsOfInputWithManyWords_Expect_WcCommandExecuteCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("wc this is just a text", fileSystem);
        assertTrue(command.executeCommand());

    }

}
