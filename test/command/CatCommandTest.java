package command;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class CatCommandTest {
    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_CallDisplayContentOnExistingFile_Expect_ToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("cat File.txt", fileSystem);
        assertTrue(command.executeCommand());
    }

    @Test
    public void When_CallDisplayContentNonExistingFile_Expect_ToReturnFalse() throws IOException {
        Command catFile = cmdFactory.getCommand("cat File.txt", fileSystem);
        assertFalse(catFile.executeCommand());
    }
}
