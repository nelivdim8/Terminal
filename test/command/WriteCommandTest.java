package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;

import exceptions.FileInvalidLineException;
import filesystem.VirtualFileSystem;

public class WriteCommandTest {
    private Command command;
    private VirtualFileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_WriteInFile_Expect_FileSizeToBeLinesNumberPlusCharactersNumberOnEachLine() throws IOException {

        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 Hello World lq", fileSystem);
        command.executeCommand();
        assertEquals(15, fileSystem.getCurrentFolder().getFile("File.txt").getSize());

    }

    @Test
    public void When_WriteInFileOnLineWhichContainCharacters_Expect_FileSizeToBecameNewTextLengthPlusLinesNumber()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 Hello World lq", fileSystem);
        command.executeCommand();
        assertEquals(15, fileSystem.getCurrentFolder().getFiles().get("File.txt").getSize());
        command = cmdFactory.getCommand("write File.txt 1 Java", fileSystem);
        command.executeCommand();
        System.out.println(
                fileSystem.getCurrentFolder().getFiles().get("File.txt").getFormattedContent(StandardCharsets.UTF_8));
        assertEquals(5, fileSystem.getCurrentFolder().getFiles().get("File.txt").getSize());

    }

    @Test
    public void When_WriteInFileDirectoryGreaterThanBounds_Expect_WriteToFileToReturnFalse() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 20 Hello World lq", fileSystem);
        command.executeCommand();
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_WriteInFileDirectoryLessThanBounds_Expect_WriteToFileToReturnFalse() throws IOException {

        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt -5 Hello World lq", fileSystem);
        command.executeCommand();
        assertFalse(command.executeCommand());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_WriteInFileOnLineWhichIsGreaterThanLinesCountPlusOne_Expect_AnExceptionToBeThrown()
            throws IOException {
        String content = "Hello World lq";
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 Hello World lq", fileSystem);
        command.executeCommand();
        assertEquals(content, fileSystem.getCurrentFolder().getFiles().get("File.txt").getContent().get(1));

    }

    @Test
    public void When_WriteInFileOnLineWhichContainCharacters_Expect_ContentToBeChangedWithNewInput()
            throws IOException {
        String newContent = "Java";
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 Hello World lq", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 Java", fileSystem);
        command.executeCommand();

        assertEquals(newContent, fileSystem.getCurrentFolder().getFiles().get("File.txt").getContent().get(0));

    }

    @Test(expected = FileInvalidLineException.class)
    public void When_WriteSpaceInEmptyLineFile_Expect_FileInvalidLineExceptionToBeThrown() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1", fileSystem);
        command.executeCommand();
        assertEquals(" ", fileSystem.getCurrentFolder().getFiles().get("File.txt").getLine(1));

    }

}
