package command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class WcLinesCommandTest {

    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_CountLinesOfExistingInputFileName_Expect_WcCommandExecuteCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one two three", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        command.executeCommand();

    }

    @Test
    public void When_CountLinesOfExistingInputFileName_Expect_WcCommandOutputToReturnLinesInFileContent()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 1 one", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 2 one", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("write File.txt 3 one", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        command.executeCommand();
        assertEquals("3", command.getOutput());
    }

    @Test
    public void When_CountLinesOfUnexistingInputFileName_Expect_WcCommandToLookAtItAsTextAndReturnItsLinesCount()
            throws IOException {

        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        command.executeCommand();
        assertEquals("1", command.getOutput());
    }

    @Test
    public void When_CountLinesOfUnexistingInputFileName_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {

        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountLinesOfExistingInputFileNameWithZeroLines_Expect_WcCommandToReturnCountZero()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        command.executeCommand();
        assertEquals("0", command.getOutput());

    }

    @Test
    public void When_CountLinesOfExistingInputFileNameWithZeroLines_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("wc -l File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountLinesOfInputWithOneWordWithInvalidForAFileSymbols_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("wc -l *&^**", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountLinesOfInputWithOneWordWithInvalidForAFileSymbols_Expect_WcCommandToLookAtItAsTextAndReturnItsLinesCount()
            throws IOException {
        command = cmdFactory.getCommand("wc -l *&^**", fileSystem);
        command.executeCommand();
        assertEquals("1", command.getOutput());

    }

    @Test
    public void When_CountLinesOfInputWithSeveralLines_Expect_WcCommandExecuteCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("wc -l this is a text\nseparated by lines\nand it should return\nfour",
                fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountLinesOfInputWithSeveralLines_Expect_WcCommandToLookAtItAsTextAndReturnItsLinesCount()
            throws IOException {
        command = cmdFactory.getCommand("wc -l this is a text\nseparated by lines\nand it should return\nfour",
                fileSystem);
        command.executeCommand();
        assertEquals("4", command.getOutput());

    }

    @Test
    public void When_CountLinesOfInputWithSeveralLinesWithSpacesAroundLineSymbol_Expect_WcCommandExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("wc -l this is a text \n separated by lines \n and it should return \n four",
                fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_CountLinesOfInputWithSeveralLinesWithSpacesAroundLineSymbol_Expect_WcCommandToLookAtItAsTextAndReturnItsLinesCount()
            throws IOException {
        command = cmdFactory.getCommand("wc -l this is a text \n separated by lines \n and it should return \n four",
                fileSystem);
        command.executeCommand();
        assertEquals("4", command.getOutput());

    }

}
