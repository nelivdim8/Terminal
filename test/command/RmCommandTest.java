package command;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import filesystem.FileSystem;
import filesystem.VirtualFileSystem;

public class RmCommandTest {
    private Command command;
    private FileSystem fileSystem;
    private CommandFactory cmdFactory;

    @Before
    public void setData() {
        fileSystem = new VirtualFileSystem();
        cmdFactory = CommandFactory.getInstance();

    }

    @Test
    public void When_RemoveFileWhichExists_Expect_removeCommandExecuteCommandToReturnTrue() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("rm File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_RemoveFileWhichDoesNotExist_Expect_removeCommandExecuteCommandToReturnFalse() throws IOException {

        command = cmdFactory.getCommand("rm File.txt", fileSystem);
        assertFalse(command.executeCommand());

    }

    @Test
    public void When_RemoveFileAndThenCreateFileWithTheSameName_Expect_CreateFileExecuteCommandToReturnTrue()
            throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("rm File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        assertTrue(command.executeCommand());

    }

    @Test
    public void When_RemoveFile_Expect_FileSystemCurrentFolderNotToContainTheRemovedFile() throws IOException {
        command = cmdFactory.getCommand("create_file File.txt", fileSystem);
        command.executeCommand();
        command = cmdFactory.getCommand("rm File.txt", fileSystem);
        command.executeCommand();
        assertFalse(fileSystem.currentFolderContainsFile("File.txt"));

    }

}
