package command;

import java.io.IOException;
import java.nio.charset.Charset;

import filesystem.FileSystem;

public class WriteCommand extends AbstractCommand {
    private String input;
    private String result;
    private final static String REGEX = "write\\s[a-zA-Z0-9._-]+\\s\\d+\\s.+(\\s[A-Z0-9-]+)?";

    private WriteCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public WriteCommand() {
        super();
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        String[] words = input.split("\\s");
        String name = words[1];
        int line = Integer.parseInt(words[2]);
        String text = "";
        if (Charset.availableCharsets().containsKey(words[words.length - 1])) {
            text = extractText(words, 3, words.length - 1);
            return writeToFile(name, line, text, words[words.length - 1]);

        }
        text = extractText(words, 3, words.length);
        return writeToFile(name, line, text);
    }

    public String extractText(String[] words, int from, int to) {
        StringBuilder sb = new StringBuilder();
        for (int i = from; i < to; i++) {
            sb.append(words[i]);
            if (i != words.length - 1) {
                sb.append(" ");
            }

        }
        return sb.toString();
    }

    private boolean writeToFile(String name, int line, String text) throws IOException {

        return super.getFileSystem().writeToFile(name, line, text);

    }

    private boolean writeToFile(String name, int line, String text, String charset) throws IOException {

        if (Charset.availableCharsets().containsKey(charset)) {
            return super.getFileSystem().writeToFile(name, line, text, Charset.forName(charset));
        }
        return false;

    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new WriteCommand(input, fileSystem);

    }

}
