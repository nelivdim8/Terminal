package command;

import filesystem.FileSystem;

public class NullCommand extends AbstractCommand {

    @Override
    public boolean executeCommand() {

        return false;
    }

    @Override
    public String getOutput() {

        return null;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {

        return null;
    }

}
