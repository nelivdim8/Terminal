package command;

import java.io.IOException;

import filesystem.FileSystem;

public class MakeDirCommand extends AbstractCommand {
    private String input;
    private final static String REGEX = "mkdir\\s(?s)[a-zA-Z0-9._-]+";

    private MakeDirCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public MakeDirCommand() {
        super();
    }

    @Override
    public boolean executeCommand() throws IOException {
        return mkDir((input.substring(6)));
    }

    private boolean mkDir(String name) throws IOException {
        return super.getFileSystem().mkDir(name);
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new MakeDirCommand(input, fileSystem);

    }

}
