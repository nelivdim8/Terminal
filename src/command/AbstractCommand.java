package command;

import filesystem.FileSystem;

public abstract class AbstractCommand implements Command<String, FileSystem> {

    private FileSystem fileSystem;

    public AbstractCommand(FileSystem fileSystem) {
        super();
        this.fileSystem = fileSystem;
    }

    public AbstractCommand() {
        super();
        this.fileSystem = null;
    }

    public FileSystem getFileSystem() {
        return fileSystem;
    }

    @Override
    public String getOutput() {

        return "";
    }

}
