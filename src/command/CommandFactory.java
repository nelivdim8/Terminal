package command;

import java.util.Map;
import java.util.TreeMap;

import filesystem.FileSystem;

public class CommandFactory {
    private final Map<String, Command> getCommandByName = new TreeMap<>();
    private static CommandFactory instance = new CommandFactory();
    static {

        CommandFactory.getInstance().addCommand(CatCommand.getRegex(), new CatCommand());
        CommandFactory.getInstance().addCommand(MakeDirCommand.getRegex(), new MakeDirCommand());
        CommandFactory.getInstance().addCommand(CdCommand.getRegex(), new CdCommand());
        CommandFactory.getInstance().addCommand(WriteCommand.getRegex(), new WriteCommand());
        CommandFactory.getInstance().addCommand(CreateFileCommand.getRegex(), new CreateFileCommand());
        CommandFactory.getInstance().addCommand(PipeCommand.getRegex(), new PipeCommand());
        CommandFactory.getInstance().addCommand(WcCommand.getRegex(), new WcCommand());
        CommandFactory.getInstance().addCommand(WcLinesCommand.getRegex(), new WcLinesCommand());
        CommandFactory.getInstance().addCommand(LsCommand.getRegex(), new LsCommand());
        CommandFactory.getInstance().addCommand(LsSortedDescCommand.getRegex(), new LsSortedDescCommand());
        CommandFactory.getInstance().addCommand(RmCommand.getRegex(), new RmCommand());
        CommandFactory.getInstance().addCommand(RemoveCommand.getRegex(), new RemoveCommand());
        CommandFactory.getInstance().addCommand(SetCharsetCommand.getRegex(), new SetCharsetCommand());
        CommandFactory.getInstance().addCommand(CompressCommand.getRegex(), new CompressCommand());

    }

    private CommandFactory() {
    }

    public Command getCommand(String input, FileSystem fileSystem) {
        for (String command : getCommandByName.keySet()) {
            if (input.matches(command)) {
                return getCommandByName.get(command).createCommand(input, fileSystem);// .apply(input,fileSystem)//define
                                                                                      // in static initializer

            }

        }
        return new NullCommand();
    }

    public void addCommand(String input, Command command) {
        getCommandByName.put(input, command);

    }

    public static CommandFactory getInstance() {

        return instance;

    }

}
