package command;

import java.io.IOException;

import filesystem.FileSystem;

public class LsSortedDescCommand extends AbstractCommand {
    private String result;
    private final static String regex = "ls --sorted desc";

    private LsSortedDescCommand(FileSystem fileSystem) {
        super(fileSystem);

    }

    public LsSortedDescCommand() {
        super();
    }

    public static String getRegex() {
        return regex;
    }

    @Override
    public boolean executeCommand() throws IOException {

        result = super.getFileSystem().listSortedFiles();
        printFiles();
        return true;
    }

    private void printFiles() {
        System.out.println("Files: \n" + result + "\n");

    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new LsSortedDescCommand(fileSystem);

    }

}
