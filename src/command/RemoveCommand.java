package command;

import java.io.IOException;

import filesystem.FileSystem;

public class RemoveCommand extends AbstractCommand {
    private final static String REGEX = "remove\\s[a-zA-Z0-9._-]+\\s[0-9]+-[0-9]+";
    private String input;

    private RemoveCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;
    }

    public RemoveCommand() {

    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws NumberFormatException, IOException {
        String[] words = input.split(" ");
        return removeLines(words);
    }

    private boolean removeLines(String[] words) throws NumberFormatException, IOException {
        String filename = words[1];
        String[] numbers = words[2].replace("-", " ").split(" ");

        return super.getFileSystem().removeLines(filename, Integer.parseInt(numbers[0]), Integer.parseInt(numbers[1]));

    }

    @Override
    public Command<String, FileSystem> createCommand(String input, FileSystem fileSystem) {
        return new RemoveCommand(input, fileSystem);
    }

}
