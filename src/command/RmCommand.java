package command;

import java.io.IOException;

import filesystem.FileSystem;

public class RmCommand extends AbstractCommand {
    private static final String REGEX = "rm\\s[a-zA-Z0-9._-]+";
    public String input;

    public RmCommand() {
        super();

    }

    private RmCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        return remove(input.substring(3));
    }

    private boolean remove(String name) throws IOException {

        return super.getFileSystem().remove(name);

    }

    @Override
    public Command<String, FileSystem> createCommand(String input, FileSystem fileSystem) {
        return new RmCommand(input, fileSystem);
    }

}
