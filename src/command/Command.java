package command;

import java.io.IOException;

public interface Command<T, R> {

    boolean executeCommand() throws IOException;

    String getOutput();

    Command<T, R> createCommand(T t, R r);

}
