package command;

import java.io.IOException;

import filesystem.FileSystem;

public class WcCommand extends AbstractCommand {
    private String input;
    private String result;
    private final static String REGEX = "wc\\s(?s).+";

    private WcCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public WcCommand() {
        super();
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        result = String.valueOf(calculateWordsCount(input));
        printWordsCount();
        return true;

    }

    private int calculateWordsCount(String text) throws IOException {
        String[] inputText = text.split(" ");
        if (inputText.length == 2) {

            return countWordsInFile(inputText[1]);

        }
        return inputText.length - 1;
    }

    private void printWordsCount() {
        System.out.println("Words count is " + result + ".");

    }

    private int countWordsInFile(String name) throws IOException {

        return super.getFileSystem().getFileWordsCount(name);

    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new WcCommand(input, fileSystem);

    }

}
