package command;

import java.io.IOException;
import java.nio.charset.Charset;

import filesystem.FileSystem;

public class SetCharsetCommand extends AbstractCommand {
    private static final String REGEX = "set\\s[a-zA-Z0-9._-]+\\s[A-Z0-9-]+";
    public String input;

    public SetCharsetCommand() {
        super();

    }

    private SetCharsetCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        String[] words = input.split(" ");
        if (Charset.isSupported(words[2])) {

            return super.getFileSystem().setFileEncoding(words[1], Charset.forName(words[2]));

        }
        return false;
    }

    @Override
    public Command<String, FileSystem> createCommand(String t, FileSystem r) {
        return new SetCharsetCommand(t, r);
    }

}
