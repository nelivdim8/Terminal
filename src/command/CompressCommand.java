package command;

import java.io.IOException;

import filesystem.FileSystem;

public class CompressCommand extends AbstractCommand {
    private final static String REGEX = "compress\\s[a-zA-Z0-9._-]+";
    private String filename;

    private CompressCommand(FileSystem fileSystem, String input) {
        super(fileSystem);
        this.filename = getFilename(input);
    }

    public CompressCommand() {
        super();

    }

    public static String getRegex() {
        return REGEX;
    }

    private String getFilename(String input) {
        String[] words = input.split(" ");
        return words[1];
    }

    @Override
    public boolean executeCommand() throws IOException {
        if (super.getFileSystem().currentFolderContainsFile(filename)) {
            try {
                return super.getFileSystem().compressFile(filename);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public Command<String, FileSystem> createCommand(String t, FileSystem r) {
        return new CompressCommand(r, t);
    }
}
