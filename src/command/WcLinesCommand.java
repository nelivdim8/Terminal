package command;

import java.io.IOException;

import filesystem.FileSystem;

public class WcLinesCommand extends AbstractCommand {
    private String input;
    private final static String NEW_LINE_SYMBOL = "\n";
    private String result;
    private final static String REGEX = "wc -l\\s(?s).+";

    private WcLinesCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public WcLinesCommand() {
        super();
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        result = String.valueOf(calculateLinesCount(input));
        printLinesCount();
        return true;
    }

    private int calculateLinesCount(String text) throws IOException {
        String[] words = text.split(" ");
        if (isAFileName(words)) {

            return countLinesOfFile(words[2]);

        }
        return countLinesOfText(text);

    }

    private int countLinesOfText(String text) {
        String[] words = text.split(NEW_LINE_SYMBOL);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        return words.length;
    }

    private int countLinesOfFile(String name) throws IOException {
        return super.getFileSystem().getFileLinesCount(name);
    }

    private void printLinesCount() {
        System.out.println("Lines count is " + result + ".");

    }

    private boolean isAFileName(String[] words) {
        return words.length == 3 && super.getFileSystem().isAFileName(words[2]);

    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {

        return new WcLinesCommand(input, fileSystem);

    }
}
