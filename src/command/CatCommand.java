package command;

import java.io.IOException;
import java.nio.charset.Charset;

import filesystem.FileSystem;

public class CatCommand extends AbstractCommand {
    private String result;
    private String input;
    private final static String REGEX = "cat\\s[a-zA-Z0-9._-]+(\\s[A-Z0-9-]+)?";

    private CatCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public CatCommand() {
        super();
    }

    @Override
    public boolean executeCommand() throws IOException {
        return displayFileContent(input);
    }

    private boolean displayFileContent(String input) throws IOException {
        String[] words = input.split(" ");

        if (words.length == 2) {
            result = super.getFileSystem().displayFileContent(words[1]);

        } else {
            if (!Charset.availableCharsets().containsKey(words[2])) {
                return false;
            }
            result = super.getFileSystem().displayFileContent(words[1], Charset.forName(words[2]));

        }

        System.out.println(result);

        return true;
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new CatCommand(input, fileSystem);

    }
    // String regex,BiFunction<String,FileSystem,CatCommand> function
    //
}
