package command;

import java.io.IOException;

import filesystem.FileSystem;

public class LsCommand extends AbstractCommand {
    private String result;
    private final static String REGEX = "ls";

    private LsCommand(FileSystem fileSystem) {
        super(fileSystem);

    }

    public LsCommand() {
        super();
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public boolean executeCommand() throws IOException {
        result = super.getFileSystem().listFiles();
        printFiles();
        return true;

    }

    private void printFiles() {
        System.out.println("Files: \n" + result + "\n");

    }

    @Override
    public String getOutput() {
        return result;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new LsCommand(fileSystem);

    }

}
