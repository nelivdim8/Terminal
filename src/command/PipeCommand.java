package command;

import java.io.IOException;

import filesystem.FileSystem;

public class PipeCommand extends AbstractCommand {
    private String input;
    private Command cmd1;
    private Command cmd2;
    private final static String REGEX = ".+\\s\\|\\s.+";

    private PipeCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public PipeCommand() {
        super();
    }

    @Override
    public boolean executeCommand() throws IOException {
        String[] separatedInput = separateInputCommands();
        return pipe(separatedInput);

    }

    private boolean pipe(String[] separatedInput) throws IOException {
        String inputCmd1 = separatedInput[0];
        String inputCmd2 = separatedInput[1];
        cmd1 = CommandFactory.getInstance().getCommand(inputCmd1, super.getFileSystem());
        boolean result = cmd1.executeCommand();
        if (result) {
            cmd2 = CommandFactory.getInstance().getCommand(inputCmd2 + " " + cmd1.getOutput(), super.getFileSystem());
            return cmd2.executeCommand();

        }
        return false;
    }

    private String[] separateInputCommands() {
        String[] commands = input.split("\\s\\|\\s");
        return commands;
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public String getOutput() {
        return cmd2.getOutput();
    }

    public Command getCmd1() {
        return cmd1;
    }

    public Command getCmd2() {
        return cmd2;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new PipeCommand(input, fileSystem);

    }

}
