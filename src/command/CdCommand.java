package command;

import filesystem.FileSystem;

public class CdCommand extends AbstractCommand {
    private String input;
    private final static String REGEX = "cd\\s[a-zA-Z0-9._-]+:*";

    private CdCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public CdCommand() {
        super();
    }

    @Override
    public boolean executeCommand() {
        return changeDir(input);
    }

    private boolean changeDir(String input) {
        return super.getFileSystem().changeDir(input.substring(3));
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new CdCommand(input, fileSystem);
    }

}
