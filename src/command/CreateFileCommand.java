package command;

import java.io.IOException;

import filesystem.FileSystem;

public class CreateFileCommand extends AbstractCommand {
    private String input;
    private final static String REGEX = "create_file\\s[a-zA-Z0-9._-]+";

    private CreateFileCommand(String input, FileSystem fileSystem) {
        super(fileSystem);
        this.input = input;

    }

    public CreateFileCommand() {
        super();
    }

    @Override
    public boolean executeCommand() throws IOException {
        return createFile(input.substring(12));
    }

    private boolean createFile(String name) throws IOException {
        return super.getFileSystem().createFile(name);
    }

    public static String getRegex() {
        return REGEX;
    }

    @Override
    public Command createCommand(String input, FileSystem fileSystem) {
        return new CreateFileCommand(input, fileSystem);

    }

}
