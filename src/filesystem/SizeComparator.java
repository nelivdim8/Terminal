package filesystem;

import java.util.Comparator;

public class SizeComparator implements Comparator<VirtualFile> {

    @Override
    public int compare(VirtualFile o1, VirtualFile o2) {
        if (o1.getSize() < o2.getSize()) {
            return 1;
        }
        if (o1.getSize() > o2.getSize()) {
            return -1;
        }
        return o1.getName().compareTo(o2.getName());
    }

}
