package filesystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

import exceptions.FileInvalidLineException;
import exceptions.UnexistingFileException;
import fileutilities.FileCompressor;

public class RealFileSystem implements FileSystem {
    private Path current;
    private Path currentRoot;
    private Map<String, Path> rootDirectories;
    private Map<Path, Charset> fileEncoding;
    private final static String FILE_SEPARATOR = System.getProperty("file.separator");

    public RealFileSystem() {
        super();
        this.rootDirectories = getRootDirectories();
        this.currentRoot = rootDirectories.get("D:");
        this.current = currentRoot;
        this.fileEncoding = new HashMap<>();

    }

    private Charset getFileEncoding(Path file) {
        if (fileEncoding.containsKey(file)) {
            return fileEncoding.get(file);
        }
        return Charset.defaultCharset();

    }

    private void setFileEncoding(Path file, Charset charset) {
        fileEncoding.put(file, charset);

    }

    public Path getCurrent() {
        return current;
    }

    public void setCurrent(Path current) {
        this.current = current;
    }

    private Map<String, Path> getRootDirectories() {
        Map<String, Path> rootDirectories = new HashMap<>();
        Iterable<Path> rootDirs = FileSystems.getDefault().getRootDirectories();
        for (Path path : rootDirs) {

            rootDirectories.put(path.toString().replace(FILE_SEPARATOR, ""), path);
        }
        return rootDirectories;

    }

    @Override
    public String getCurrentFolderName() {
        return current.toString();
    }

    @Override
    public boolean currentFolderContainsFile(String name) {
        return Files.exists(current.resolve(name));
    }

    @Override
    public boolean writeToFile(String name, int line, String text) throws IOException {
        return writeToFile(name, line, text, getFileEncoding(current.resolve(name)));

    }

    private boolean setLines(List<String> lines, int line, String text) throws FileInvalidLineException {
        if (line > lines.size()) {
            throw new FileInvalidLineException("Line " + line + " is invalid!");
        }

        if (line == lines.size()) {
            lines.add(line, text);

        } else {
            lines.set(line, text);
        }
        return true;

    }

    @Override
    public String displayFileContent(String name) throws UnexistingFileException, IOException {
        return displayFileContent(name, getFileEncoding(current.resolve(name)));
    }

    private List<String> getFileContent(String name, Charset charset) throws IOException {

        List<String> list = new ArrayList<>();
        try (FileInputStream is = new FileInputStream(current.resolve(name).toFile());
                InputStreamReader isr = new InputStreamReader(is, charset);
                BufferedReader reader = new BufferedReader(isr)) {
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                list.add(line);
            }
        }

        return list;

    }

    @Override
    public boolean changeDir(String name) {
        if (name.equals(".")) {
            return true;
        }
        if (isARootDirectory(name)) {
            setCurrent(getRootDirectory(name));
        }
        if (name.equals("..")) {
            if (current.equals(currentRoot)) {
                return false;
            }
            setCurrent(current.getParent());
            return true;
        }
        if (Files.isDirectory(current.resolve(name))) {
            setCurrent(current.resolve(name));
            return true;
        }

        return false;

    }

    private boolean isARootDirectory(String name) {
        return rootDirectories.containsKey(name);
    }

    private Path getRootDirectory(String name) {
        return rootDirectories.get(name);
    }

    @Override
    public boolean createFile(String name) throws IOException {
        if (!Files.isRegularFile(current.resolve(name))) {

            Files.createFile(current.resolve(name));
            return true;

        }
        return false;

    }

    @Override
    public boolean mkDir(String name) throws IOException {
        if (current.toString().endsWith(name)) {
            return false;
        }
        Files.createDirectories(current.resolve(name));

        return true;
    }

    @Override
    public boolean removeLines(String filename, int from, int to) throws IOException {
        if (!Files.isRegularFile(current.resolve(filename))) {
            throw new UnexistingFileException("File with name " + filename + " does not exitst");
        }
        if (from < 0 || to < 0 || from > to) {
            throw new FileInvalidLineException("Line " + from + " or line " + to + " is invalid!");

        }
        Path path = current.resolve(filename);
        List<String> lines;

        lines = Files.readAllLines(path);
        removeLines(lines, from, to);
        Files.write(path, lines);
        return true;

    }

    private void removeLines(List<String> lines, int from, int to) throws FileInvalidLineException {
        if (to >= lines.size()) {
            throw new FileInvalidLineException("Line " + to + " is invalid!");
        }
        for (int i = from; i <= to; i++) {

            lines.remove(from);
        }

    }

    @Override
    public boolean remove(String name) throws IOException {
        if (!Files.isRegularFile(current.resolve(name))) {
            throw new UnexistingFileException("File with name " + name + " does not exitst");
        }

        Files.delete(current.resolve(name));
        return true;
    }

    @Override
    public int getFileWordsCount(String name) throws UnexistingFileException, FileNotFoundException {
        return countInFile(name, x -> x.hasNext(), x -> x.next());
    }

    @Override
    public int getFileLinesCount(String name) throws UnexistingFileException, FileNotFoundException {
        return countInFile(name, x -> x.hasNextLine(), x -> x.nextLine());
    }

    private int countInFile(String name, Predicate<Scanner> predicate, Function<Scanner, String> function)
            throws UnexistingFileException, FileNotFoundException {
        if (!Files.isRegularFile(current.resolve(name))) {
            throw new UnexistingFileException("File with name " + name + " does not exitst");
        }
        File file = current.resolve(name).toFile();
        int counter = 0;
        try (Scanner input = new Scanner(new BufferedReader(new FileReader(file)))) {

            while (predicate.test(input)) {
                function.apply(input);
                counter++;

            }
        }

        return counter;

    }

    @Override
    public String listSortedFiles() throws IOException {
        List<File> files = new ArrayList<>();

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(current)) {
            for (Path file : stream) {
                if (Files.isRegularFile(file))
                    files.add(file.toFile());
            }

        }
        sortFiles(files);
        return filesToString(files, x -> x.getName());

    }

    private void sortFiles(List<File> files) {

        files.sort((f1, f2) -> (int) f2.length() - (int) f1.length());

    }

    @Override
    public String listFiles() throws IOException {
        StringBuilder sb = new StringBuilder();

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(current)) {
            for (Path file : stream) {
                if (Files.isRegularFile(file))
                    sb.append(file.getFileName()).append("\n");
            }

        }
        return sb.toString();
    }

    private String filesToString(List<File> files, Function<File, ?> getField) {
        StringBuilder sb = new StringBuilder();
        for (File file : files) {
            sb.append(getField.apply(file)).append("\n");
        }
        return sb.toString();

    }

    private String getFilesFormattedContent(List<String> content) {
        StringBuilder sb = new StringBuilder();
        for (String line : content) {
            sb.append(line).append("\n");
        }
        return sb.toString();

    }

    @Override
    public boolean isAFileName(String name) {

        return Files.isRegularFile(current.resolve(name));
    }

    @Override
    public boolean setFileEncoding(String name, Charset charset) throws IOException {
        if (!Files.isRegularFile(current.resolve(name))) {
            throw new UnexistingFileException("File with name " + name + " does not exitst");
        }

        Path path = current.resolve(name);
        if (getFileEncoding(path).equals(charset)) {
            return false;
        }
        List<String> content = getFileContent(name, getFileEncoding(path));
        writeListToFile(path, content, charset);
        setFileEncoding(path, charset);
        return true;

    }

    @Override
    public boolean writeToFile(String name, int line, String text, Charset charset) throws IOException {
        if (!Files.isRegularFile(current.resolve(name))) {
            throw new UnexistingFileException("File with name " + name + " does not exitst");
        }
        if (line < 0) {
            throw new FileInvalidLineException();
        }
        Path path = current.resolve(name);
        List<String> lines = getFileContent(name, getFileEncoding(path));
        setLines(lines, line, text);
        writeListToFile(path, lines, charset);
        setFileEncoding(path, charset);
        return true;

    }

    private void writeListToFile(Path path, List<String> list, Charset charset) throws IOException {

        try (FileOutputStream is = new FileOutputStream(path.toFile());
                OutputStreamWriter isr = new OutputStreamWriter(is, charset);
                BufferedWriter writer = new BufferedWriter(isr)) {
            for (String line : list) {
                writer.append(changeLineEncoding(line, getFileEncoding(path), charset));
                writer.newLine();
            }
        }

    }

    private String changeLineEncoding(String text, Charset oldEncoding, Charset newEncoding) {
        byte bytes[] = text.getBytes(oldEncoding);
        return new String(bytes, newEncoding);
    }

    @Override
    public String displayFileContent(String name, Charset charset) throws IOException {
        if (!Files.isRegularFile(current.resolve(name))) {
            throw new UnexistingFileException("File with name " + name + " does not exitst");
        }
        return getFilesFormattedContent(getFileContent(name, charset));
    }

    @Override
    public boolean compressFile(String filename) throws IOException, InterruptedException {
        FileCompressor.compress(current.resolve(filename));
        return true;
    }

}
