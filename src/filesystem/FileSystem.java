package filesystem;

import java.io.IOException;
import java.nio.charset.Charset;

public interface FileSystem {
    String getCurrentFolderName();

    boolean currentFolderContainsFile(String name);

    boolean writeToFile(String name, int line, String text) throws IOException;

    boolean writeToFile(String name, int line, String text, Charset charset) throws IOException;

    String displayFileContent(String name) throws IOException;

    String displayFileContent(String name, Charset charset) throws IOException;

    boolean changeDir(String name);

    boolean createFile(String name) throws IOException;

    boolean mkDir(String name) throws IOException;

    boolean removeLines(String filename, int from, int to) throws IOException;

    boolean remove(String name) throws IOException;

    int getFileWordsCount(String name) throws IOException;

    int getFileLinesCount(String name) throws IOException;

    String listSortedFiles() throws IOException;

    String listFiles() throws IOException;

    boolean isAFileName(String name);

    boolean setFileEncoding(String name, Charset charset) throws IOException;

    boolean compressFile(String filename) throws IOException, InterruptedException;

}
