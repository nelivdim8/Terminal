package filesystem;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import exceptions.FileInvalidLineException;

public class VirtualFile implements Comparable<VirtualFile> {
    private List<String> content;
    private String name;
    private int size;
    private Charset encoding;

    public VirtualFile(String name) {
        super();
        this.content = new ArrayList<>();
        this.name = name;
        this.size = content.size();
        this.encoding = Charset.defaultCharset();

    }

    public List<String> getContent() {
        return content;
    }

    public String getLine(int number) throws FileInvalidLineException {
        if (number > 0 && number <= content.size()) {
            return content.get(number - 1);
        }
        throw new FileInvalidLineException("Invalid line");
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public boolean setEncoding(Charset encoding) {
        if (changeContentEncoding(encoding)) {
            this.encoding = encoding;
            return true;
        }
        return false;

    }

    public String getFormattedContent(Charset charset) {
        if (content.size() == 0) {
            return "Empty file!";
        }
        if (charset.equals(encoding)) {
            return getContent(charset, x -> x);
        }
        return getContent(charset, x -> changeLineEncoding(x, charset));
    }

    private String getContent(Charset charset, Function<String, String> function) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < content.size(); i++) {
            sb.append((i + 1) + ": " + " " + function.apply(content.get(i)));

        }
        return sb.toString();
    }

    private boolean changeContentEncoding(Charset charset) {
        if (encoding.equals(charset)) {
            return false;
        }

        for (int i = 0; i < content.size(); i++) {
            content.set(i, changeLineEncoding(content.get(i), charset));

        }
        return true;
    }

    private String changeLineEncoding(String text, Charset charset) {
        byte bytes[] = text.getBytes(encoding);
        return new String(bytes, charset);
    }

    public boolean write(int line, String text) throws FileInvalidLineException {
        return write(line, text, encoding);
    }

    public boolean write(int line, String text, Charset charset) throws FileInvalidLineException {
        if (!isValidLine(line)) {
            throw new FileInvalidLineException("Invalid line!");
        }
        if (isUnexistingLine(line)) {
            content.add(line - 1, text);
            size++;
        } else {
            String contentOnLine = content.get(line - 1);
            size -= contentOnLine.length();
            content.set(line - 1, text);
        }
        size += text.length();
        changeContentEncoding(charset);
        return true;
    }

    public int getSize() {
        return size;
    }

    public int getLinesCount() {
        return content.size();
    }

    public int countWords() {
        int count = 0;
        for (String textOnLine : content) {
            count += countWords(textOnLine);
        }
        return count;
    }

    private int countWords(String text) {
        String[] words = text.split(" ");
        return words.length;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + size;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VirtualFile other = (VirtualFile) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (size != other.size)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name + " ";
    }

    @Override
    public int compareTo(VirtualFile o) {
        if (this.name.compareTo(o.name) > 0) {
            return 1;
        }
        if (this.name.compareTo(o.name) < 0) {
            return -1;
        }
        return 0;
    }

    public boolean removeContent(int from, int to) throws FileInvalidLineException {
        if (from > to || from > content.size() + 1 || to > content.size() + 1 || from - 1 < 0 || to - 1 < 0) {
            throw new FileInvalidLineException("Invalid line indexes!");
        }
        int indexToRemove = from - 1;
        for (int i = from; i <= to; i++) {
            removeLine(indexToRemove);
        }
        return true;

    }

    public int getLineSize(int line) {
        return content.get(line).length() + 1;

    }

    public void removeLine(int indexToRemove) {
        size -= getLineSize(indexToRemove);
        content.remove(indexToRemove);

    }

    private boolean isValidLine(int line) {
        return (line - 1 >= 0 && line - 1 < content.size() + 1);
    }

    private boolean isUnexistingLine(int line) {
        return ((line == 1 && content.size() == 0) || line == content.size() + 1);
    }

    public int getLineSize(int line, String text) throws FileInvalidLineException {
        if (isValidLine(line)) {
            if (isUnexistingLine(line)) {
                return text.length() + 1;
            } else {
                return text.length() - content.get(line - 1).length();
            }
        }

        throw new FileInvalidLineException("Invalid line");

    }

}
