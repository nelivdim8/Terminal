package filesystem;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import exceptions.FileInvalidLineException;
import exceptions.HardDriveOutOfStorageException;
import exceptions.UnexistingFileException;
import harddrive.Capacity;
import harddrive.HardDrive;

public class VirtualFileSystem implements FileSystem {
    private Folder root;
    private Folder current;
    private HardDrive hardDrive = new HardDrive(Capacity.TWENTY);
    private List<VirtualFile> bin;

    public VirtualFileSystem() {
        super();
        this.root = new Folder("/");
        this.current = new Folder("home");
        this.current.setParent(root);
        this.root.addSubfolder(current);
        this.bin = new ArrayList<>();
    }

    private String getCurrentFolderName(Folder folder) {
        if (folder == root) {
            return folder.getName();

        }
        return getCurrentFolderName(folder.getParent()) + folder.getName() + "/";
    }

    public String getCurrentFolderName() {

        return getCurrentFolderName(current);
    }

    public Folder getCurrentFolder() {
        return current;
    }

    public Folder getRoot() {
        return root;
    }

    public void setCurrent(Folder current) {
        this.current = current;
    }

    public VirtualFile getCurrentFolderFile(String name) {
        return current.getFile(name);
    }

    public boolean currentFolderContainsFile(String name) {
        return current.containsFile(name);
    }

    public String getCurrentFolderFileContent(String name, Charset charset) {
        return current.getFile(name).getFormattedContent(charset);
    }

    public boolean isCurrentFolderRoot() {
        return current.equals(root);
    }

    public Folder getCurrentFolderParent() {
        return current.getParent();
    }

    private void addFileToCurrentFolder(String name) throws HardDriveOutOfStorageException {
        if (hardDrive.isFill()) {
            if (bin.isEmpty()) {
                throw new HardDriveOutOfStorageException();
            }
            hardDrive.freeMemory(clearBin());

        }
        current.addFile(name);

    }

    public boolean writeToFile(String name, int line, String text)
            throws HardDriveOutOfStorageException, UnexistingFileException, FileInvalidLineException {
        if (!current.containsFile(name)) {
            throw new UnexistingFileException("File with name " + name + " does not exist!");
        }
        int changeInSize = current.getFile(name).getLineSize(line, text);
        if (!hardDrive.hasEnoughMemory(changeInSize)) {
            if (!hardDrive.hasEnoughMemory(changeInSize - getBinSize())) {
                throw new HardDriveOutOfStorageException("You does not have enough space to write changes!");
            }
            hardDrive.freeMemory(clearBin());
        }

        boolean isSuccessful = current.getFile(name).write(line, text);
        hardDrive.takeMemory(changeInSize);
        return isSuccessful;

    }

    public String displayFileContent(String name) throws UnexistingFileException {
        if (!currentFolderContainsFile(name)) {
            throw new UnexistingFileException("File does not exist!");

        }
        return getCurrentFolderFileContent(name, current.getFile(name).getEncoding());

    }

    public boolean changeDir(String name) {
        if (name.equals(".")) {
            return true;
        }
        if (name.equals("..")) {
            if (current.equals(root)) {
                return false;
            }
            setCurrent(current.getParent());
            return true;
        }
        if (current.containsSubFolder(name)) {
            setCurrent(current.getSubFolder(name));
            return true;
        }

        return false;
    }

    public boolean createFile(String name) throws HardDriveOutOfStorageException {
        if (!current.containsFile(name)) {

            addFileToCurrentFolder(name);

            return true;
        }

        return false;
    }

    public boolean mkDir(String name) {
        if (name.equals(current.getName()) || current.containsSubFolder(name)) {
            return false;
        }

        current.addSubfolder(name);
        return true;
    }

    public boolean removeLines(String filename, int from, int to)
            throws UnexistingFileException, FileInvalidLineException {

        if (!current.containsFile(filename)) {
            throw new UnexistingFileException("File to remove lines from does not exist!");
        }
        return current.getFile(filename).removeContent(from, to);

    }

    public boolean remove(String name) throws UnexistingFileException {

        bin.add(current.removeFile(name));
        return true;

    }

    public int getFileWordsCount(String name) throws UnexistingFileException {
        if (!current.containsFile(name)) {
            throw new UnexistingFileException("File does not exist!");
        }
        return current.getFile(name).countWords();

    }

    public int getFileLinesCount(String name) throws UnexistingFileException {
        if (!current.containsFile(name)) {
            throw new UnexistingFileException("File does not exist");
        }
        return current.getFile(name).getLinesCount();

    }

    public String listSortedFiles() {
        List<VirtualFile> files = current.listFiles();
        if (files.size() < 1) {
            return "0";

        }
        List<VirtualFile> sorted = current.listFiles();
        return getFormattedFileNames(sorted);

    }

    private String getFormattedFileNames(List<VirtualFile> files) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < files.size(); i++) {
            sb.append(files.get(i));
            if (i < files.size() - 1) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public String listFiles() {
        List<VirtualFile> files = current.listFiles();
        if (files.size() < 1) {
            return "0";

        }
        return getFormattedFileNames(files);

    }

    private int clearBin() {
        int freeMemory = 0;
        for (VirtualFile file : bin) {
            freeMemory += file.getSize() + 1;

        }
        bin.clear();
        return freeMemory;
    }

    private int getBinSize() {
        int size = 0;
        for (VirtualFile file : bin) {
            size += file.getSize();
        }
        return size;
    }

    @Override
    public boolean isAFileName(String name) {
        return current.containsFile(name);
    }

    @Override
    public boolean writeToFile(String name, int line, String text, Charset charset) throws IOException {
        return writeToFile(name, line, text, charset);
    }

    @Override
    public String displayFileContent(String name, Charset charset) throws IOException {
        if (!currentFolderContainsFile(name)) {
            throw new UnexistingFileException("File does not exist!");

        }
        return getCurrentFolderFileContent(name, charset);

    }

    @Override
    public boolean setFileEncoding(String name, Charset charset) throws UnexistingFileException {
        if (!current.containsFile(name)) {
            throw new UnexistingFileException("File with name " + name + " does not exist!");
        }
        return current.getFile(name).setEncoding(charset);
    }

    @Override
    public boolean compressFile(String filename) throws IOException {
        // TODO Auto-generated method stub
        return false;
    }

}
