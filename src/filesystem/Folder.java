package filesystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exceptions.UnexistingFileException;

public class Folder {
    private Folder parent;
    private Map<String, Folder> subfolders;
    private Map<String, VirtualFile> files;
    private String name;

    public Folder(String name, Folder parent) {
        super();
        this.parent = parent;
        this.subfolders = new HashMap<>();
        this.files = new HashMap<>();
        this.name = name;

    }

    public Folder(String name) {
        super();
        this.parent = null;
        this.subfolders = new HashMap<>();
        this.files = new HashMap<>();
        this.name = name;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public Map<String, Folder> getSubfolders() {
        return subfolders;
    }

    public void addSubfolder(Folder subfolder) {
        subfolder.setParent(this);
        this.subfolders.put(subfolder.getName(), subfolder);
    }

    public void addSubfolder(String name) {

        this.subfolders.put(name, new Folder(name, this));
    }

    public void addFile(String name) {

        this.files.put(name, new VirtualFile(name));
    }

    public Folder getSubFolder(String name) {
        return this.subfolders.get(name);
    }

    public VirtualFile getFile(String name) {
        return files.get(name);
    }

    public boolean containsSubFolder(String name) {
        return this.subfolders.containsKey(name);
    }

    public boolean containsSubFolders() {
        return !this.subfolders.isEmpty();
    }

    public boolean containsFiles() {
        return !this.files.isEmpty();
    }

    public boolean containsFile(String name) {
        return this.files.containsKey(name);
    }

    public Map<String, VirtualFile> getFiles() {
        return files;
    }

    public void addFile(VirtualFile file) {

        this.files.put(file.getName(), file);
    }

    public VirtualFile removeFile(String name) throws UnexistingFileException {
        if (!containsFile(name)) {
            throw new UnexistingFileException("File to remove does not exist!");
        }

        return this.files.remove(name);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VirtualFile> listSortedFiles() {
        List<VirtualFile> files = new ArrayList<>(this.files.values());

        Collections.sort(files, new SizeComparator());

        return files;
    }

    public List<VirtualFile> listFiles() {
        List<VirtualFile> files = new ArrayList<>(this.files.values());
        Collections.sort(files);

        return files;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((files == null) ? 0 : files.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((subfolders == null) ? 0 : subfolders.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Folder other = (Folder) obj;
        if (files == null) {
            if (other.files != null)
                return false;
        } else if (!files.equals(other.files))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parent == null) {
            if (other.parent != null)
                return false;
        } else if (!parent.equals(other.parent))
            return false;
        if (subfolders == null) {
            if (other.subfolders != null)
                return false;
        } else if (!subfolders.equals(other.subfolders))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Folder: name=" + name + "\n";
    }

}
