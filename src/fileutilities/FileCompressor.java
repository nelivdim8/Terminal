package fileutilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;

public class FileCompressor {

    private static ForkJoinPool forkJoinPool = new ForkJoinPool();

    private static List<String> readFile(Path path) throws IOException {
        List<String> content = new ArrayList<>();
        try (Scanner input = new Scanner(new BufferedReader(new FileReader(path.toFile())))) {
            while (input.hasNext())
                content.add(input.next());

        }

        return content;

    }

    private static void writeToFile(Path path, int[] result) throws IOException {

        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path.toFile())))) {
            for (int i = 0; i < result.length; i++) {
                writer.print(result[i]);
                writer.print(" ");

            }
        }

    }

    private static Map<String, Integer> countWordsOccurences(List<String> words) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            if (map.containsKey(words.get(i))) {
                map.put(words.get(i), map.get(words.get(i)) + 1);
            } else {
                map.put(words.get(i), 1);
            }

        }

        return map;
    }

    private static List<Integer> getMaxOccurence(Map<String, Integer> map) throws InterruptedException {
        int[] values = new int[map.values().size()];
        int index = 0;
        for (Integer i : map.values()) {
            values[index] = i;
            index++;
        }
        forkJoinPool.invoke(new SortNumbersTask(values, 0, values.length));
        List<Integer> sorted = new ArrayList<Integer>();
        for (int j = 0; j < values.length; j++) {

            sorted.add(values[j]);
        }
        return sorted;

    }

    public static void compress(Path path) throws IOException, InterruptedException {
        List<String> words = readFile(path);
        Map<String, Integer> map = countWordsOccurences(words);
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
        }
        List<Integer> sorted = getMaxOccurence(map);
        boolean[] visited = new boolean[sorted.size()];
        calculateCompressedValues(map, sorted, visited);
        writeToFile(path, convertToDigits(words, map));
    }

    private static void calculateCompressedValues(Map<String, Integer> map, List<Integer> sorted, boolean[] visited) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            map.put(entry.getKey(), calculateCompressedValue(entry, sorted, visited));

        }

    }

    private static int calculateCompressedValue(Map.Entry<String, Integer> entry, List<Integer> sorted,
            boolean[] visited) {
        int index = sorted.indexOf(entry.getValue());
        if (!visited[index]) {
            visited[index] = true;
        } else {
            while (visited[index]) {
                index++;
            }
            visited[index] = true;

        }
        return index;

    }

    private static int[] convertToDigits(List<String> words, Map<String, Integer> map) throws InterruptedException {
        int[] result = new int[words.size()];
        forkJoinPool.invoke(new WordsToDigitsConvertorTask(map, words, result, 0, words.size()));
        return result;
    }

}
