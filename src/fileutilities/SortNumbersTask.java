package fileutilities;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;

public class SortNumbersTask extends RecursiveAction {
    private int[] digits;
    private int from;
    private int to;
    private final static int THRESHOLD = 10000;

    public SortNumbersTask(int[] digits, int from, int to) {
        super();
        this.digits = digits;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void compute() {

        if (to - from < THRESHOLD) {
            simpleSorting();
        } else {
            int middle = (from + to) / 2;
            SortNumbersTask task1 = new SortNumbersTask(digits, from, middle);
            task1.fork();
            SortNumbersTask task2 = new SortNumbersTask(digits, middle + 1, to);
            task2.fork();
            merge(from, middle, to, digits);

        }
    }

    private void simpleSorting() {
        int key;
        for (int i = from + 1; i < to; i++) {
            key = digits[i];
            int j = i - 1;
            while (j >= 0 && digits[j] < key) {
                digits[j + 1] = digits[j];
                j--;
            }
            digits[j + 1] = key;
        }
    }

    public static void merge(int start, int middle, int end, int[] array) {
        int[] array1 = Arrays.copyOfRange(array, start, middle + 1);
        int[] array2 = Arrays.copyOfRange(array, middle + 1, end + 1);
        int k1 = 0;
        int k2 = 0;
        int j = start;

        while (k1 < array1.length && k2 < array2.length) {
            if (array1[k1] > array2[k2]) {
                array[j++] = array1[k1++];

            } else {

                array[j++] = array2[k2++];

            }

        }

        while (k1 < array1.length) {
            array[j++] = array1[k1++];

        }
        while (k2 < array2.length) {
            array[j++] = array2[k2++];

        }

    }

}
