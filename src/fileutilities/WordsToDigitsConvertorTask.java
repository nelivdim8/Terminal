package fileutilities;

import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveAction;

public class WordsToDigitsConvertorTask extends RecursiveAction {
    private Map<String, Integer> map;
    private List<String> words;
    private int[] result;
    private int from;
    private int to;
    private final static int THRESHOLD = 1000;

    public WordsToDigitsConvertorTask(Map<String, Integer> map, List<String> words, int[] result, int from, int to) {
        super();
        this.map = map;
        this.words = words;
        this.from = from;
        this.to = to;
        this.result = result;
    }

    public void simpleExecution() {
        for (int i = from; i < to; i++) {
            result[i] = map.get(words.get(i));

        }

    }

    @Override
    protected void compute() {
        int length = to - from;
        if (length < THRESHOLD) {
            simpleExecution();
        } else {
            int delta = length / 2;
            WordsToDigitsConvertorTask task1 = new WordsToDigitsConvertorTask(map, words, result, from, from + delta);
            task1.fork();
            WordsToDigitsConvertorTask task2 = new WordsToDigitsConvertorTask(map, words, result, from + delta, to);
            task2.fork();

        }

    }

}
