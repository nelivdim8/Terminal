package start;

import filesystem.RealFileSystem;
import terminal.Terminal;

public class Start {
    public static void main(String[] args) {
        Terminal terminal = new Terminal(new RealFileSystem());

        terminal.open();

    }
}
