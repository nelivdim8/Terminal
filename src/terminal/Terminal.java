package terminal;

import java.io.IOException;

import command.Command;
import command.CommandFactory;
import filesystem.FileSystem;
import userinput.UserInput;

public class Terminal {
    private UserInput input;
    private FileSystem fileSystem;

    public Terminal(FileSystem fileSystem) {
        super();
        this.input = new UserInput();
        this.fileSystem = fileSystem;

    }

    public String getCommand() {
        return input.getInput();
    }

    public boolean executeCommand(Command command) {
        try {
            return command.executeCommand();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    public void open() {
        while (true) {
            showTerminal();
            String command = getCommand();
            boolean isSuccessfullyExecuted = executeCommand(
                    CommandFactory.getInstance().getCommand(command, fileSystem));
            if (!isSuccessfullyExecuted) {
                System.out.println("Cannot execute!");
            }

        }

    }

    public void showTerminal() {
        System.out.println();
        System.out.print(fileSystem.getCurrentFolderName());
    }

}
