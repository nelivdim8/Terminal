package userinput;

import java.util.Scanner;

public class UserInput {
    private Scanner input;

    public UserInput() {
        this.input = new Scanner(System.in);
    }

    public String getInput() {

        return input.nextLine();
    }

}
