package exceptions;

import java.io.IOException;

public class HardDriveOutOfStorageException extends IOException {

    public HardDriveOutOfStorageException() {
        super();

    }

    public HardDriveOutOfStorageException(String message) {
        super(message);

    }

}
