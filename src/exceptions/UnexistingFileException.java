package exceptions;

import java.io.IOException;

public class UnexistingFileException extends IOException {

    public UnexistingFileException() {
        super();

    }

    public UnexistingFileException(String message) {
        super(message);

    }

}
