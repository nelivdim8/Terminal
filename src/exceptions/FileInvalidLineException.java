package exceptions;

import java.io.IOException;

public class FileInvalidLineException extends IOException {

    public FileInvalidLineException() {
        super();

    }

    public FileInvalidLineException(String message) {
        super(message);

    }

}
