package harddrive;

public enum Capacity {
    FIFTY(50), TWENTY(20), TEN(10), FIVE(5);
    private int size;

    private Capacity(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
