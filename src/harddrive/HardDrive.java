package harddrive;

public class HardDrive {
    private Capacity max;
    private int capacity;
    private int freeMemory;

    public HardDrive(Capacity max) {
        super();
        this.max = max;
        this.capacity = max.getSize();
        this.freeMemory = capacity;

    }

    public Capacity getMax() {
        return max;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getFreeMemory() {
        return freeMemory;
    }

    public boolean isFill() {
        return freeMemory == 0;
    }

    public int takeMemory(int size) {
        return freeMemory = (capacity < size || size < 0) ? 0 : freeMemory - size;
    }

    public int freeMemory(int size) {
        return freeMemory = (capacity < size || size < 0) ? 0 : freeMemory + size;
    }

    public boolean hasEnoughMemory(int addition) {
        return freeMemory - addition >= 0;
    }

}
